<?php
/**
 * Created by PhpStorm.
 * User: qingniao
 * Date: 2018/5/15
 * Time: 15:22
 */
/*生产混淆码*/
function get_salt($length=-6)
{
    return substr(uniqid(rand()), $length);
}


//加密地址返回参数
function return_par($data)
{
    $return_url = base64_decode(urldecode($data));
    $chuci = explode('&',$return_url);
    if(count($chuci) == 1){
        return [];
    }

    $arr = array();
    foreach ($chuci as $key=>$value){
        $temp = explode('=',$value);
        $arr[$temp['0']] = $temp['1'];
    }

    return $arr;
}


/**
 * 手机号正则验证
 */
function check_mobile($mobile){
    if(preg_match("/^0{0,1}(13[0-9]|15[0-9]|18[0-9]|14[0-9]|17[0-9]|19[0-9])[0-9]{8}$/",$mobile)){
        return true;
    }else{
        return false;
    }
}
