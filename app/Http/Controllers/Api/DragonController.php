<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-28
 * Time: 17:40
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\MemberBasic;
use App\Models\MemberBasicInfo;
use App\Models\MemberDragon;
use App\Models\MemberEnterprise;
use App\Models\ServiceProject;
use Illuminate\Http\Request;

class DragonController extends Controller {
    public function getBasics() {
        return response()->json(['code' => '1', 'message' => '获取成功', 'data' => $this->basiceType()]);
    }


    public function basiceType() {
        $temp['nature_unit'] = [
            '1' => '党政机关',
            '2' => '事业单位',
            '3' => '企业或其他经济社会组织',
        ];

        $temp['area_range'] = ['九龙坡区内','九龙坡区外'];
        /*$temp['nationality'] = [
            '1' => '中国',
            '2' => '日本',
            '3' => '美国',
            '4' => '英国',
            '5' => '德国',
        ];*/

        $temp['talents_type'] = [
            '1' => '专业技术人才',
            '2' => '技能人才',
            '3' => '管理人才',
        ];

        $temp['xueli'] = [
            '1' => '大专及以下',
            '2' => '本科',
            '3' => '硕士',
            '4' => '博士',
        ];

        $temp['xuewei'] = [
            '1' => '学士',
            '2' => '硕士研究生',
            '3' => '博士研究生',
            '4' => '无',
        ];

        $temp['professional_field'] = [
            '1'  => '新能源',
            '2'  => '新材料',
            '3'  => '生物技术',
            '4'  => '新医药',
            '5'  => '节能环保',
            '6'  => '软件和服务外包',
            '7'  => '物联网和新一代信息技术',
            '8'  => '纳米技术',
            '9'  => '文化产业',
            '10' => '现代农业',
            '11' => '光机电一体化',
            '12' => '电子信息',
            '13' => '医疗',
            '14' => '教育',
            '15' => '管理',
            '16' => '经济贸易',
            '17' => '其他',
        ];

        $temp['introduce_way'] = [
            '1' => '刚性引进',
            '2' => '柔性引进',
        ];

        $temp['enterprise_type'] = [
            '国有企业', '民营企业', '有限合伙', '中外合资', '其他',
        ];
        $service                 = new ServiceProject();

        $info = $service->where('status', '1')->get(['id', 'title']);
        foreach ($info as $key => $value) {
            $temp['service_project'][$value->id] = $value->title;
        }

        return $temp;
    }


    public function applyDragonCard(Request $request) {
        $data      = $request->input();
        $bool_data = $this->getValidator($data);
        if ($bool_data['code'] == 0) {
            $result['code']    = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }

        $memberModel = new Member();
        $member      = $memberModel->getMember(['id' => $data['user']['uid']], ['id', 'is_check']);
        if (!in_array($member->is_check, ['0', '4', '5'])) {
            return response()->json(['code' => '0', 'message' => '您不能提交']);
        }

        $dragonModel = new MemberDragon();
        $dragon_info = $dragonModel->getDragon(['uid' => $data['user']['uid']], ['is_check', 'is_freeze', 'freeze_time']);
        if (!empty($dragon_info)) {
            if ($dragon_info->is_check == 5 && $dragon_info->freeze_time > time()) {
                return response()->json(['code' => '0', 'message' => '冻结时间未到，您不能修改']);
            }
        }

        $qiyeModel = new MemberEnterprise();
        $qiyeModel->addEnterprise($bool_data['data']);

        $basicModel = new MemberBasic();
        $basicModel->addBasic($bool_data['data']);

        $basicInfoModel = new MemberBasicInfo();
        $basicInfoModel->addBasicInfo($data);

        $dragonModel = new MemberDragon();
        $dragonModel->addDragon($bool_data['data']);

        if ($memberModel->updMember(['id' => $data['user']['uid']], ['is_check' => 2])) {
            $message = new MessageController();
            $message->addMessage($data['user']['uid'], '1', '人才龙卡申请待审核', ['is_check' => '2']);
            return response()->json(['code' => '1', 'message' => '申请成功']);
        } else {
            return response()->json(['code' => '0', 'message' => '申请失败']);
        }
    }


    /**
     * 验证
     * @param $data
     * @param $type 0 通过手机号验证码注册
     * @return array
     */
    public function getValidator($data, $type = 0) {
        $validator = \Validator::make($data, [
            'enterprise_name'     => 'required',
            'area_range'          => 'required',
            'nature_unit'         => 'required',
            'firm_name'           => 'required',
            //'registered_capital'=>'required',
            'annual_value'        => 'required',
            'enterprise_address'  => 'required',
            'lianxiren'           => 'required',
            'enterprise_mobile'   => 'required',
            'enterprise_type'     => 'required',
            'name'                => 'required',
            'mobile'              => 'required|integer',
            'identity_num'        => 'required',
            //'birthplace'=>'required',
//            'jz_address'=>'required',
//            'jz_area_range'=>'required',
            'nationality'         => 'required',
            'hk_address'          => 'required',
            'xueli'               => 'required',
//            'talents_type'        => 'required',
            'graduate_school'     => 'required',
            'graduate_time'       => 'required',
            'technical_level'     => 'required',
            'duty'                => 'required',
            'professional_field'  => 'required',
            'introduce_way'       => 'required',
//            'come_district_time'=>'required',
            'personal_experience' => 'required',
            //'performance'=>'required',
            'avatar'              => 'required',
            'service_project'     => 'required',
            'xuewei'              => 'required',
            'img'                 => 'required',
        ], [
            'required' => ':attribute为必填项',
            'max'      => ':attribute长度不符合要求',
            'integer'  => ':attribute必须为数字',
        ], [
            'enterprise_name'     => '企业名称',
            'area_range'          => '单位区域',
            'nature_unit'         => '单位性质',
            'firm_name'           => '行业名称',
            //'registered_capital'=>'注册资金',
            'annual_value'        => '年产值',
            'enterprise_address'  => '企业地址',
            'lianxiren'           => '企业联系人',
            'enterprise_mobile'   => '企业联系电话',
            'enterprise_type'     => '企业类型',
            'name'                => '姓名',
            'mobile'              => '电话',
            'identity_num'        => '身份证号码',
            //'birthplace'=>'出生地',
//            'jz_address'=>'居住地',
//            'jz_area_range'=>'居住区域',
            'nationality'         => '国籍',
            'hk_address'          => '户口所在地',
            'xueli'               => '学历',
            'xuewei'              => '学位',
//            'talents_type'        => '人才类型',
            'graduate_school'     => '毕业院校',
            'graduate_time'       => '毕业时间',
            'technical_level'     => '职称',
            'duty'                => '职务',
            'professional_field'  => '专业领域',
            'introduce_way'       => '引进方式',
//            'come_district_time'=>'来区时间',
            'personal_experience' => '学习与工作经历',
            //'performance'=>'个人业绩',
            'avatar'              => '个人头像',
            'service_project'     => '服务项目',
            'img'                 => '相关资料',
        ]);


        //验证失败，并返回第一个报错
        if ($validator->fails()) {
            return ['code' => 0, 'message' => $validator->errors()->first()];
        }
        $basicType = $this->basiceType();
        if (!in_array($data['enterprise_type'], $basicType['enterprise_type']))
            return ['code' => 0, 'message' => '请选择正确的企业类型：国有企业、民营企业、有限合伙、中外合资、其他'];
        if (!in_array($data['area_range'], $basicType['area_range']))
            return ['code' => 0, 'message' => '请选择正确的单位区域：九龙坡区内、九龙坡区外'];
        //手机号验证
        if (!check_mobile($data['mobile'])) {
            return ['code' => '0', 'message' => '请输入正确的手机号'];
        }

        if (is_file(app_path() . DIRECTORY_SEPARATOR . 'Libraries' . DIRECTORY_SEPARATOR . 'Idcard' . DIRECTORY_SEPARATOR . 'Idcard.php')) {
            require_once app_path() . DIRECTORY_SEPARATOR . 'Libraries' . DIRECTORY_SEPARATOR . 'Idcard' . DIRECTORY_SEPARATOR . 'Idcard.php';
        }

        $Idcard = new \Idcard();
        if (!$Idcard->isChinaIDCard($data['identity_num'])) {
            return ['code' => '0', 'message' => "身份证号码错误：{$data['identity_num']}"];
        }

        $data['birthday'] = $Idcard->birthday;
        $sex              = $Idcard->getChinaIDCardSex($data['identity_num']);
        if ($sex == '男') {
            $data['sex'] = 1;
        } else {
            $data['sex'] = 2;
        }


        if ($data['introduce_way'] == 1) {
            if (empty($data['service_contract_time'])) {
                return ['code' => '0', 'message' => '劳务合同签订时间不能为空'];
            }

            //社保购买地址
            if (empty($data['social_security_address'])) {
                return ['code' => '0', 'message' => '社保购买地不能为空'];
            }
        } else {
            if (empty($data['introduce_way_describe'])) {
                return ['code' => '0', 'message' => '引进方式不能为空'];
            }
        }

        if ($type == 0) {
            if (empty($data['code'])) {
                return ['code' => '0', 'message' => '验证码不能为空'];
            }

            $code = new NoteCodeController();
            $bool = $code->verifyCode(['code' => $data['code'], 'mobile' => $data['mobile']]);
            if ($bool['code'] == 0) {
                return $bool;
            }
        }


        //$data['service_project'] = implode(',',$data['service_project']);
//        $data['service_project'] = $data['service_project'];


        //荣誉内容
        if (empty($data['honor_content'])) {
            $data['honor_content'] = '';
        }

        //出身地
        if (empty($data['birthplace'])) {
            $data['birthplace'] = '';
        }

        //荣誉等级
        if (empty($data['honor_grade'])) {
            $data['honor_grade'] = '';
        }

        if (empty($data['registered_capital'])) {
            $data['registered_capital'] = '';
        }

        if (empty($data['annual_value'])) {
            $data['annual_value'] = '';
        }

        if (empty($data['personal_experience'])) {
            $data['personal_experience'] = '';
        }

        if (empty($data['performance'])) {
            $data['performance'] = '';
        }
        return ['code' => 1, 'message' => '验证成功', 'data' => $data];
    }

    /**
     * 进度查询
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reviewProgress(Request $request) {
        $user              = $request->input('user');
        $memberDragonModel = new MemberDragon();

        $dragon = $memberDragonModel->getDragon(['uid' => $user['uid']], [
            'uid',
            'is_check',
            //'first_trial_user',
            'first_trial_time',
            'first_remark',
            //'second_trial_user',
            'second_trial_time',
            'second_remark',
            'is_freeze',
            'freeze_time',
        ]);
        if (empty($dragon)) {
            return response()->json(['code' => '0', 'message' => '您还未提交申请', 'data' => [
                'uid'               => $user['uid'],
                'is_check'          => 0,
                'first_trial_time'  => '',
                'first_remark'      => '',
                'second_trial_time' => '',
                'second_remark'     => '',
            ]]);
        }


        if (!empty($dragon->first_trial_time)) {
            $dragon->first_trial_time = date('Y/m/d H:i:s', $dragon->first_trial_time);
        } else {
            $dragon->first_trial_time = '';
        }

        if (!empty($dragon->second_trial_time)) {
            $dragon->second_trial_time = date('Y/m/d H:i:s', $dragon->second_trial_time);
        } else {
            $dragon->second_trial_time = '';
        }

        if (!empty($dragon->freeze_time)) {
            $dragon->freeze_time = date('Y/m/d', $dragon->freeze_time);
        } else {
            $dragon->freeze_time = '';
        }

        return response()->json(['code' => '1', 'message' => '获取成功', 'data' => $dragon]);
    }

    public function getDetails(Request $request) {
        $data = $request->input();

        $memberModel = new Member();
        $member      = $memberModel->getDetails($data['user']['uid']);
        return response()->json(['code' => '1', 'message' => '获取成功', 'data' => $member]);
    }
}
