<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SuggestFeedback;
use Illuminate\Http\Request;

class AdminSuggestController extends Controller
{
    public function list(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);
        $suggestModel = new SuggestFeedback();
        $list = $suggestModel->getSuggestFeedbackList($request->input());
        return response()->json(['code'=>'1','message'=>'请求成功','data'=>$list]);
    }
}
