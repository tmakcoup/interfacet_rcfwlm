<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class AdminPcImgController extends Controller
{
    public function getImg()
    {
        $info = DB::table('pc_indeximg')->where('id','1')->first();
        if(empty($info)){
            $img = [];
        }else{
            $img = json_decode($info->img,true);
        }
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>['img'=>$img]]);
    }

    public function subImg(Request $request)
    {
        if(!$request->has('img')){
            return response()->json(['code'=>'0','message'=>'请上传图片']);
        }
        $info = DB::table('pc_indeximg')->where('id','1')->first();

        $temp['img'] = json_encode($request->input('img'));
        $temp['updtime'] = time();
        if(empty($info)){
            $bool = DB::table('pc_indeximg')->insert($temp);
        }else{
            $bool = DB::table('pc_indeximg')->where('id','1')->update($temp);
        }

        if($bool){
            return response()->json(['code'=>'1','message'=>'上传成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'上传失败']);
        }
    }
}
