<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ReleasePolicy;
use Illuminate\Http\Request;

class ReleasePolicyController extends Controller
{
    protected $releaseModel;

    public function __construct()
    {
        $this->releaseModel = new ReleasePolicy();
    }

    public function list(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);

        $data = $request->input();
        $list = $this->releaseModel->getReleasePolicyList($data);
        return response()->json($list);
    }



    public function weixinList(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);


        $data = $request->input();
        if(empty($data['type'])){
            return response()->json(['code'=>'0','message'=>'请上传列表类型']);
        }

        $list = $this->releaseModel->getReleasePolicyList($data);
        return response()->json($list);
    }

    public function weixinDetails(Request $request)
    {
        $data = $request->input();
        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'请选择政策发布']);
        }

        $info = $this->releaseModel->getReleasePolicy(['id'=>$data['id'],'status'=>1]);
        if(empty($info)){
            return response()->json(['code'=>'0','message'=>'请选择政策发布']);
        }

        $info->addtime = date('Y-m-d H:i:s',$info->addtime);
        unset($info->updtime);
        
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$info]);
    }


    /**
     * 详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function details(Request $request)
    {
        $data = $request->input();
        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'请选择政策发布']);
        }

        $info = $this->releaseModel->getReleasePolicy(['id'=>$data['id'],'status'=>1]);
        if(empty($info)){
            return response()->json(['code'=>'0','message'=>'请选择政策发布']);
        }


        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$info]);
    }

    /**
     * 更新
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updReleasePolicy(Request $request)
    {
        $data = $request->input();
        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'请上传政策发布id']);
        }

        $info = $this->releaseModel->getReleasePolicy(['id'=>$data['id']]);
        if(empty($info)){
            return response()->json(['code'=>'0','message'=>'请选择政策发布']);
        }

        $bool_data = $this->getValidator($data);
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }
        $bool = $this->releaseModel->updReleasePolicy(['id'=>$data['id']],$bool_data['data']);
        return response()->json($bool);
    }

    /**
     * 添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addReleasePolicy(Request $request)
    {
        $bool_data = $this->getValidator($request->input());
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }
        $bool = $this->releaseModel->addReleasePolicy($bool_data['data']);
        return response()->json($bool);
    }


    /**
     * 验证
     * @param $data
     * @return array
     */
    public function getValidator($data)
    {
        $validator = \Validator::make($data,[
            'type'=>'required',
            'title'=>'required',
            'img'=>'required',
            'content'=>'required',
        ],[
            'required'=>':attribute为必填项',
            'max'=>':attribute长度不符合要求',
            'integer'=>':attribute必须为数字',
        ],[
            'type'=>'政策类别',
            'title'=>'标题',
            'img'=>'图片',
            'content'=>'内容详情',
        ]);

        //验证失败，并返回第一个报错
        if($validator->fails()) {
            return ['code' => 0, 'message' => $validator->errors()->first()];
        }

        $temp['type'] = implode(',',$data['type']);
        $temp['title'] = $data['title'];
        $temp['img'] = $data['img'];
        $temp['content'] = $data['content'];
        $temp['addtime'] = time();
        $temp['updtime'] = $temp['addtime'];
        $temp['fb_user'] = $data['user']['uid'];

        return ['code'=>1,'message'=>'验证成功','data'=>$temp];
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $data = $request->input();
        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'请上传政策发布id']);
        }

        $info = $this->releaseModel->getReleasePolicy(['id'=>$data['id'],'status'=>'1']);
        if(empty($info)){
            return response()->json(['code'=>'0','message'=>'请选择政策发布']);
        }

        $bool = $this->releaseModel->updReleasePolicy(['id'=>$data['id']],['status'=>'0','updtime'=>time()]);
        if($bool['code'] == 1){
            $result = ['code'=>'1','message'=>'删除成功'];
        }else{
            $result = ['code'=>'0','message'=>'删除失败 '];
        }
        return response()->json($result);
    }

    /**
     * 排序
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpDown(Request $request)
    {
        $data = $request->input();
        if(empty($data['type'])){
            return response()->json(['code'=>'0','message'=>'请上传排序类型']);
        }

        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'请上传政策服务id']);
        }

        $release = $this->releaseModel
            ->where('id',$data['id'])
            ->where('status',1)
            ->first();
        if(empty($release)){
            return response()->json(['code'=>'0','message'=>'政策服务不存在']);
        }

        $rank = $release->rank;
        if($data['type'] == 1){
            if($rank == 1){
                $result['code'] = '0';
                $result['message'] = '此政策服务已经是最优先了';
                return response()->json($result);
            }

            $release_up = $this->releaseModel
                ->where('status','1')
                ->where('rank','<',$rank)
                ->orderBy('rank','desc')
                ->first();
            if(empty($release_up)){
                $result['code'] = '0';
                $result['message'] = '此政策服务已经是最优先了';
                return response()->json($result);
            }

            $this->releaseModel->where('id',$data['id'])->where('status',1)->update(['rank'=>$release_up->rank,'updtime'=>time()]);
            $this->releaseModel->where('id',$release_up->id)->where('status',1)->update(['rank'=>$rank,'updtime'=>time()]);
            $result['code'] = '1';
            $result['message'] = '优先成功';
            return response()->json($result);
        }else{
            $release_dow = $this->releaseModel
                ->where('status','1')
                ->where('rank','>',$rank)
                ->orderBy('rank','asc')
                ->first();

            if(empty($release_dow)){
                $result['code'] = '0';
                $result['message'] = '此政策服务已经是最末尾了';
                return response()->json($result);
            }

            $this->releaseModel->where('id',$data['id'])->where('status',1)->update(['rank'=>$release_dow->rank,'updtime'=>time()]);
            $this->releaseModel->where('id',$release_dow->id)->where('status',1)->update(['rank'=>$rank,'updtime'=>time()]);
            $result['code'] = '1';
            $result['message'] = '滞后成功';
            return response()->json($result);
        }
    }
}
