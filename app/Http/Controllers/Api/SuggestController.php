<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SuggestFeedback;
use Illuminate\Http\Request;


class SuggestController extends Controller
{
    public function tijiao(Request $request)
    {
        $data = $request->input();
        $bool_data = $this->getValidator($data);
        if($bool_data['code'] == 0){
            return response()->json(['code'=>'0','message'=>$bool_data['message']]);
        }

        $suggesModel = new SuggestFeedback();
        $bool = $suggesModel->addSuggestFeedback($bool_data['data']);
        if($bool){
            return response()->json(['code'=>'1','message'=>'提交成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'提交失败']);
        }
    }


    /**
     * 验证
     * @param $data
     * @return array
     */
    public function getValidator($data)
    {
        $validator = \Validator::make($data,[
            'title'=>'required',
            'content'=>'required',
            'img'=>'required',
        ],[
            'required'=>':attribute为必填项',
            'max'=>':attribute长度不符合要求',
            'integer'=>':attribute必须为数字',
        ],[
            'title'=>'手机号',
            'content'=>'内容',
            'img'=>'图片',
        ]);


        //验证失败，并返回第一个报错
        if($validator->fails()) {
            return ['code' => 0, 'message' => $validator->errors()->first()];
        }

        $temp['title'] = $data['title'];
        $temp['uid'] = $data['user']['uid'];
        $temp['content'] = $data['content'];
        $temp['title'] = $data['title'];
        $temp['img'] = json_encode($data['img'],true);
        $temp['addtime'] = time();

        return ['code'=>1,'message'=>'验证成功','data'=>$temp];
    }
}
