<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-25
 * Time: 11:38
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use OSS\OssClient;
use OSS\Core\OssException;

class ImageController extends Controller
{
    protected $validType = [
        'bmp','jpg','jpeg','png','pdf','doc','docx','xlsx','xls'
    ];
    public function uploadImg(Request $request)
    {
        if(!$request->has('file')){
            $result = [
                'code' => '0',
                'message' => '请上传图片',
            ];

            return response()->json($result);
        }


        $file = $request->file('file');

        //判断文件是否上传成功
        if($file->isValid()){
            //原文件名
            $originaName = $file->getClientOriginalName();
            //扩展名
            $ext = $file->getClientOriginalExtension();
            //文件类型
            $type = $file->getClientMimeType();
            if(!in_array(strtolower($ext),$this->validType)){
                $types = implode(',',$this->validType);
                return response()->json( $result = ['code' => '0','message' => '文件格式不支持']);
            }
            //临时文件绝对路径
            $realPath = $file->getRealPath();
            //文件大小

            $size = $file->getClientSize();



            $path = $this->getTypePath(1);

            if($path['code'] == 0){
                $result = [
                    'code' => '0',
                    'message' => '图片上传路径有误',
                ];

                return response()->json($result);
            }

            $pathName = $path['path'].uniqid().'.'.$ext;
            $aly = $this->AliyunOss($pathName,file_get_contents($realPath));
            if($aly['info']['http_code'] != 200){
                return response()->json([
                    'message'=>'上传失败',
                    'code'=>'0',
                ]);
            }


            $result['code'] = '1';
            $result['message'] = '上传成功';
            $result['name'] = $originaName;
            $result['path'] = $aly['oss-request-url'];

            return response()->json($result);
        }else{
            return response()->json([
                'message'=>'上传失败',
                'code'=>'0',
            ]);
        }
    }

    public function AliyunOss($name,$content,$type = 0)
    {
        if (is_file(app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Aliyun-oss'.DIRECTORY_SEPARATOR.'autoload.php')) {
            require_once app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Aliyun-oss'.DIRECTORY_SEPARATOR.'autoload.php';
        }

        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        $accessKeyId = "LTAIKSeZmyYSdn4Z";
        $accessKeySecret = "k33JSmnxYYybzVMTzrubP3sJIBOKc8";
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        $endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
        // 存储空间名称
        $bucket= "rcfwlm-data";

        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            if($type == 0){
                return $ossClient->putObject($bucket, $name, $content);
            }else{
                return $ossClient->uploadFile($bucket, $name, $content);
            }
        } catch (OssException $e) {
            print $e->getMessage();
        }
    }

    public function getTypePath($type)
    {
        if($type == 1){
            $result['code'] = 1;
            $result['path'] = "rcjh-images/uploads/images/".date('Y-m-d')."/";
        }elseif ($type == 2){
            $result['code'] = 1;
            $result['path'] = "rcjh-images/uploads/images/".date('Y-m-d')."/";
        }elseif ($type == 3){
            $result['code'] = 1;
            $result['path'] = "rcjh-excel/uploads/excel/".date('Y-m-d')."/";
        }else{
            $result['code'] = 0;
        }

        return $result;
    }
}
