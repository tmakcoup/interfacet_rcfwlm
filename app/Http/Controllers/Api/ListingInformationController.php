<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use OSS\OssClient;
use OSS\Core\OssException;

class ListingInformationController extends Controller
{
    public function editCommunity(Request $request) {
        if (!$request->isMethod('POST')) {
            return response()->json(['code' => 0, 'message' => '提交方式错误']);
        }

        $request_data = $request->input();

        if (!isset($request_data['community_name']) || !$request_data['community_name']) {
            return response()->json(['code' => 0, 'message' => '小区名称为必填项']);
        }
        if (!isset($request_data['total_building_num']) || !$request_data['total_building_num']) {
            return response()->json(['code' => 0, 'message' => '总栋数为必填项']);
        }
        if (!isset($request_data['unit']) || !$request_data['unit']) {
            return response()->json(['code' => 0, 'message' => '楼栋单位为必填项']);
        }
        if (!in_array($request_data['unit'], array('栋', '座', '幢'))) {
            return response()->json(['code' => 0, 'message' => '楼栋单位错误']);
        }
        if (!isset($request_data['province']) || !$request_data['province']) {
            return response()->json(['code' => 0, 'message' => '省份为必填项']);
        }
        if (!isset($request_data['city']) || !$request_data['city']) {
            return response()->json(['code' => 0, 'message' => '城市为必填项']);
        }
        if (!isset($request_data['area']) || !$request_data['area']) {
            return response()->json(['code' => 0, 'message' => '地区为必填项']);
        }
        if (!isset($request_data['address']) || !$request_data['address']) {
            return response()->json(['code' => 0, 'message' => '地址为必填项']);
        }

        $result = [];
        $data = $request_data;
        unset($data['user']);
        if (!isset($request_data['community_id']) || !$request_data['community_id']) {
        	if (DB::table('community')->insert($data)) {
        		$result['code'] = 1;
                $result['message'] = '添加成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '添加失败';
            }
        } else {
        	if (DB::table('community')->where('community_id', trim($data['community_id']))->update($data)) {
        		$result['code'] = 1;
                $result['message'] = '修改成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '修改失败或内容没作修改';
            }
        }

        return response()->json($result);
    }

    public function deleteCommunity(Request $request) {
    	if (!$request->isMethod('POST')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	if (!isset($request_data['community_id']) || !$request_data['community_id']) {
	        return response()->json(['code' => 0, 'message' => '小区ID为必填项']);
    	}
    	$community_id = trim($request_data['community_id']);
    	if (DB::table('building')->where('community_id', $community_id)->where('status', '1')->first()) {
	        return response()->json(['code' => 0, 'message' => '该小区下有楼栋数据，不能删除']);
    	}
    	if (DB::table('house_type')->where('community_id', $community_id)->where('status', '1')->first()) {
	        return response()->json(['code' => 0, 'message' => '该小区下有户型数据，不能删除']);
    	}
    	if (DB::table('room_no')->where('community_id', $community_id)->where('status', '1')->first()) {
	        return response()->json(['code' => 0, 'message' => '该小区下有房号数据，不能删除']);
    	}
    	if (DB::table('community')->where('community_id', $community_id)->update(['status' => 0])) {
    		$result['code'] = 1;
            $result['message'] = '删除成功';
    	} else {
            $result['code'] = 0;
            $result['message'] = '删除失败或该数据已删除';
        }
    	return response()->json($result);
    }

    public function communityInfo(Request $request, $community_id) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}
    	$response_data = DB::table('community')->where('community_id', $community_id)->where('status', '1')->first();
    	$response_data = $response_data ? $response_data : array();
    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function communityList(Request $request) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	$community_name = '';
    	if (isset($request_data['community_name'])) {
    		$community_name = $request_data['community_name'];
    	}
        $page_size = 10;
        if (isset($request_data['page_size'])) {
            $page_size = $request_data['page_size'];
        }

    	$response_data = DB::table('community')->where('community_name', 'LIKE', '%' . $community_name . '%')->orderby('add_time', 'DESC')->where('status', '1')->Paginate($page_size);

    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function editBuilding(Request $request) {
        if (!$request->isMethod('POST')) {
            return response()->json(['code' => 0, 'message' => '提交方式错误']);
        }

        $request_data = $request->input();

        if (!isset($request_data['community_id']) || !$request_data['community_id']) {
            return response()->json(['code' => 0, 'message' => '小区ID为必填项']);
        }

        // TODO 验证小区ID

        if (!isset($request_data['buildings']) || !$request_data['buildings']) {
            return response()->json(['code' => 0, 'message' => '栋数为必填项']);
        }
        if (!isset($request_data['unit']) || !$request_data['unit']) {
            return response()->json(['code' => 0, 'message' => '楼栋单位为必填项']);
        }
        if (!in_array($request_data['unit'], array('栋', '座', '幢'))) {
            return response()->json(['code' => 0, 'message' => '楼栋单位错误']);
        }
        if (!isset($request_data['units']) || !$request_data['units']) {
            return response()->json(['code' => 0, 'message' => '总单元数为必填项']);
        }
        if (!isset($request_data['floors']) || !$request_data['floors']) {
            return response()->json(['code' => 0, 'message' => '总楼层为必填项']);
        }
        if (!is_numeric($request_data['floors'])) {
            return response()->json(['code' => 0, 'message' => '总楼层必须为数字']);
        }
        if (!isset($request_data['households']) || !$request_data['households']) {
            return response()->json(['code' => 0, 'message' => '每层总户数为必填项']);
        }
        if (!is_numeric($request_data['households'])) {
            return response()->json(['code' => 0, 'message' => '每层总户数必须为数字']);
        }

        $result = [];
        $data = $request_data;
        unset($data['user']);
        if (!isset($request_data['building_id']) || !$request_data['building_id']) {
	        if (DB::table('building')->where('community_id', $data['community_id'])->where('buildings', $data['buildings'])->where('unit', $data['unit'])->where('status', 1)->first()) {
	            return response()->json(['code' => 0, 'message' => '该小区下已有该楼栋，请勿重复添加']);
	        }
        	if (DB::table('building')->insert($data)) {
        		$result['code'] = 1;
                $result['message'] = '添加成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '添加失败';
            }
        } else {
	        if (DB::table('building')->where('community_id', $data['community_id'])->where('buildings', $data['buildings'])->where('unit', $data['unit'])->where('building_id', '<>', trim($data['building_id']))->where('status', 1)->first()) {
	            return response()->json(['code' => 0, 'message' => '该小区下已有该楼栋，请勿重复添加']);
	        }
        	if (DB::table('building')->where('building_id', trim($data['building_id']))->update($data)) {
        		$result['code'] = 1;
                $result['message'] = '修改成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '修改失败或内容没作修改';
            }
        }

        return response()->json($result);
    }

    public function deleteBuilding(Request $request) {
    	if (!$request->isMethod('POST')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	if (!isset($request_data['building_id']) || !$request_data['building_id']) {
	        return response()->json(['code' => 0, 'message' => '楼栋ID为必填项']);
    	}
    	$building_id = $request_data['building_id'];
    	if (DB::table('room_no')->where('building_id', $building_id)->where('status', '1')->first()) {
	        return response()->json(['code' => 0, 'message' => '该楼栋下有房号数据，不能删除']);
    	}
    	if (DB::table('building')->where('building_id', trim($building_id))->update(['status' => 0])) {
    		$result['code'] = 1;
            $result['message'] = '删除成功';
    	} else {
            $result['code'] = 0;
            $result['message'] = '删除失败或该数据已删除';
        }
    	return response()->json($result);
    }

    public function buildingList(Request $request) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	$community_name = '';
    	$community_id = 0;
    	if (isset($request_data['community_name'])) {
    		$community_name = $request_data['community_name'];
    	}
    	if (isset($request_data['community_id'])) {
    		$community_id = $request_data['community_id'];
    	}
        $page_size = 10;
        if (isset($request_data['page_size'])) {
            $page_size = $request_data['page_size'];
        }
        $sql = "SELECT rb.building_id, rc.community_name, rb.buildings, rb.unit, rb.units, rb.floors, rb.households, rb.add_time FROM rcjh_building rb, rcjh_community rc WHERE rb.community_id = rc.community_id AND rb.status = 1";
        if ($community_name) {
        	$sql .= " AND rc.community_name like '%$community_name%'";
        }
        if ($community_id) {
        	$sql .= " AND rc.community_id = $community_id";
        }
        $sql .= " ORDER BY rb.add_time DESC";
    	$response_data = DB::table(DB::raw("($sql limit 9999999) as buildings"))->Paginate($page_size);
    	foreach ($response_data as $key => $value) {
    		$response_data[$key]->building = $value->buildings.$value->unit;
    		$response_data[$key]->floors = $value->floors.'层';
    		$response_data[$key]->households = $value->households.'户';
    	}

    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function buildingInfo(Request $request, $building_id) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}
    	$response_data = DB::table('building')->where('building_id', $building_id)->where('status', '1')->first();
    	$response_data = $response_data ? $response_data : array();
    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function editHouseType(Request $request) {
        if (!$request->isMethod('POST')) {
            return response()->json(['code' => 0, 'message' => '提交方式错误']);
        }

        $request_data = $request->input();

        if (!isset($request_data['community_id']) || !$request_data['community_id']) {
            return response()->json(['code' => 0, 'message' => '小区ID为必填项']);
        }

        // TODO 验证小区ID

        if (!isset($request_data['house_type_name']) || !$request_data['house_type_name']) {
            return response()->json(['code' => 0, 'message' => '户型名称为必填项']);
        }
        if (!isset($request_data['rooms']) || !$request_data['rooms']) {
            return response()->json(['code' => 0, 'message' => '室为必填项']);
        }
        if (!isset($request_data['halls']) || !$request_data['halls']) {
            return response()->json(['code' => 0, 'message' => '厅为必填项']);
        }
        if (!isset($request_data['guards']) || !$request_data['guards']) {
            return response()->json(['code' => 0, 'message' => '卫为必填项']);
        }
        if (!isset($request_data['construction_area']) || !$request_data['construction_area']) {
            return response()->json(['code' => 0, 'message' => '建筑面积为必填项']);
        }

        $result = [];
        $data = $request_data;
        unset($data['user']);
        if (!isset($request_data['house_type_id']) || !$request_data['house_type_id']) {
			if (DB::table('house_type')->where('community_id', $data['community_id'])->where('house_type_name', $data['house_type_name'])->where('status', 1)->first()) {
			    return response()->json(['code' => 0, 'message' => '该小区下已有该户型名称，请勿重复添加']);
			}
        	if (DB::table('house_type')->insert($data)) {
        		$result['code'] = 1;
                $result['message'] = '添加成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '添加失败';
            }
        } else {
			if (DB::table('house_type')->where('community_id', $data['community_id'])->where('house_type_name', $data['house_type_name'])->where('house_type_id', '<>', trim($data['house_type_id']))->where('status', 1)->first()) {
			    return response()->json(['code' => 0, 'message' => '该小区下已有该户型名称，请勿重复添加']);
			}
        	if (DB::table('house_type')->where('house_type_id', trim($data['house_type_id']))->update($data)) {
        		$result['code'] = 1;
                $result['message'] = '修改成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '修改失败或内容没作修改';
            }
        }

        return response()->json($result);
    }

    public function deleteHouseType(Request $request) {
    	if (!$request->isMethod('POST')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	if (!isset($request_data['house_type_id']) || !$request_data['house_type_id']) {
	        return response()->json(['code' => 0, 'message' => '户型ID为必填项']);
    	}
    	$house_type_id = $request_data['house_type_id'];
    	if (DB::table('room_no')->where('house_type_id', $house_type_id)->where('status', '1')->first()) {
	        return response()->json(['code' => 0, 'message' => '该户型下有房号数据，不能删除']);
    	}
    	if (DB::table('house_type')->where('house_type_id', trim($house_type_id))->update(['status' => 0])) {
    		$result['code'] = 1;
            $result['message'] = '删除成功';
    	} else {
            $result['code'] = 0;
            $result['message'] = '删除失败或该数据已删除';
        }
    	return response()->json($result);
    }

    public function houseTypeList(Request $request) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	$community_name = '';
    	$community_id = 0;
    	if (isset($request_data['community_name'])) {
    		$community_name = $request_data['community_name'];
    	}
    	if (isset($request_data['community_id'])) {
    		$community_id = $request_data['community_id'];
    	}
        $page_size = 10;
        if (isset($request_data['page_size'])) {
            $page_size = $request_data['page_size'];
        }
        $sql = "SELECT rh.house_type_id, rc.community_name, rh.house_type_name, rh.halls, rh.guards, rh.rooms, rh.construction_area, rh.add_time FROM rcjh_house_type rh, rcjh_community rc WHERE rh.community_id = rc.community_id AND rh.status = 1";
        if ($community_name) {
        	$sql .= " AND rc.community_name like '%$community_name%'";
        }
        if ($community_id) {
        	$sql .= " AND rc.community_id = $community_id";
        }
        $sql .= " ORDER BY rh.add_time DESC";
    	$response_data = DB::table(DB::raw("($sql limit 9999999) as buildings"))->Paginate($page_size);
    	foreach ($response_data as $key => $value) {
    		$response_data[$key]->house_type = $value->rooms.'室'.$value->halls.'厅'.$value->guards.'卫';
    	}

    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function houseTypeInfo(Request $request, $house_type_id) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}
    	$response_data = DB::table('house_type')->where('house_type_id', $house_type_id)->where('status', '1')->first();
    	$response_data = $response_data ? $response_data : array();
    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function editRoomNo(Request $request) {
        if (!$request->isMethod('POST')) {
            return response()->json(['code' => 0, 'message' => '提交方式错误']);
        }

        $request_data = $request->input();

        if (!isset($request_data['community_id']) || !$request_data['community_id']) {
            return response()->json(['code' => 0, 'message' => '小区ID为必填项']);
        }

        // TODO 验证小区ID

        if (!isset($request_data['building_id']) || !$request_data['building_id']) {
            return response()->json(['code' => 0, 'message' => '楼栋为必填项']);
        }

        // TODO 验证楼栋ID

        if (!isset($request_data['units']) || !$request_data['units']) {
            return response()->json(['code' => 0, 'message' => '单元数为必填项']);
        }
        if (!isset($request_data['house_type_id']) || !$request_data['house_type_id']) {
            return response()->json(['code' => 0, 'message' => '户型为必填项']);
        }

        // TODO 验证户型ID

        if (!isset($request_data['floors']) || !$request_data['floors']) {
            return response()->json(['code' => 0, 'message' => '楼层数为必填项']);
        }
        if (!isset($request_data['room_no']) || !$request_data['room_no']) {
            return response()->json(['code' => 0, 'message' => '房号为必填项']);
        }

        $result = [];
        $data = $request_data;
        unset($data['user']);
        if (!isset($request_data['room_no_id']) || !$request_data['room_no_id']) {
	        if (DB::table('room_no')->where('community_id', $data['community_id'])->where('building_id', $data['building_id'])->where('units', $data['units'])->where('floors', $data['floors'])->where('room_no', $data['room_no'])->where('status', 1)->first()) {
	            return response()->json(['code' => 0, 'message' => '该小区下同一楼栋同一单元同一楼层已有该房号，请勿重复添加']);
	        }
        	if (DB::table('room_no')->insert($data)) {
        		$result['code'] = 1;
                $result['message'] = '添加成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '添加失败';
            }
        } else {
	        if (DB::table('room_no')->where('community_id', $data['community_id'])->where('building_id', $data['building_id'])->where('units', $data['units'])->where('floors', $data['floors'])->where('room_no', $data['room_no'])->where('room_no_id', '<>', $data['room_no_id'])->where('status', 1)->first()) {
	            return response()->json(['code' => 0, 'message' => '该小区下同一楼栋同一单元同一楼层已有该房号，请勿重复添加']);
	        }
        	if (DB::table('room_no')->where('room_no_id', trim($data['room_no_id']))->update($data)) {
        		$result['code'] = 1;
                $result['message'] = '修改成功';
        	} else {
                $result['code'] = 0;
                $result['message'] = '修改失败或内容没作修改';
            }
        }

        return response()->json($result);
    }

    public function deleteRoomNo(Request $request) {
    	if (!$request->isMethod('POST')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	if (!isset($request_data['room_no_id']) || !$request_data['room_no_id']) {
	        return response()->json(['code' => 0, 'message' => '房号ID为必填项']);
    	}
    	$room_no_id = $request_data['room_no_id'];
    	if (DB::table('room_no')->where('room_no_id', trim($room_no_id))->update(['status' => 0])) {
    		$result['code'] = 1;
            $result['message'] = '删除成功';
    	} else {
            $result['code'] = 0;
            $result['message'] = '删除失败或该数据已删除';
        }
    	return response()->json($result);
    }

    public function roomNoList(Request $request) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
    	$community_name = '';
    	$building_id = 0;
    	$units = '';
    	$floors = '';
    	$room_status = '';
    	if (isset($request_data['community_name'])) {
    		$community_name = $request_data['community_name'];
    	}
    	if (isset($request_data['building_id'])) {
    		$building_id = $request_data['building_id'];
    	}
    	if (isset($request_data['units'])) {
    		$units = $request_data['units'];
    	}
    	if (isset($request_data['floors'])) {
    		$floors = $request_data['floors'];
    	}
    	if (isset($request_data['room_status'])) {
    		$room_status = $request_data['room_status'];
    	}
        $page_size = 10;
        if (isset($request_data['page_size'])) {
            $page_size = $request_data['page_size'];
        }

        $sql = "SELECT rr.room_no_id, rc.community_name, rr.building_id, rr.units, rr.room_no, rr.house_type_id, rr.add_time,rr.floors FROM rcjh_room_no rr, rcjh_community rc WHERE rr.community_id = rc.community_id AND rr.status = 1";
        if ($community_name) {
        	$sql .= " AND rc.community_name like '%$community_name%'";
        }
        if ($building_id) {
        	$sql .= " AND rr.building_id = $building_id";
        }
        if ($units) {
        	$sql .= " AND rr.units = '$units'";
        }
        if ($floors) {
        	$sql .= " AND rr.floors = $floors";
        }
        if (strlen($room_status)) {
        	$sql .= " AND rr.room_status = $room_status";
        }
        $sql .= " ORDER BY rr.add_time DESC";

    	$response_data = DB::table(DB::raw("($sql limit 9999999) as buildings"))->Paginate($page_size);
    	foreach ($response_data as $key => $value) {
    		$building_info = DB::table('building')->where('building_id', $value->building_id)->first();
    		$house_type_info = DB::table('house_type')->where('house_type_id', $value->house_type_id)->first();
    		unset($response_data[$key]->building_id);
    		unset($response_data[$key]->house_type_id);
    		$response_data[$key]->buildings = $building_info->buildings.$building_info->unit;
    		$response_data[$key]->house_type_name = $house_type_info->house_type_name;
    		$response_data[$key]->construction_area = $house_type_info->construction_area;
    		$response_data[$key]->house_type = $house_type_info->rooms.'室'.$house_type_info->halls.'厅'.$house_type_info->guards.'卫';
    	}

    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function roomNoInfo(Request $request, $room_no_id) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}
    	$response_data = DB::table('room_no')->where('room_no_id', $room_no_id)->where('status', '1')->first();
    	$response_data = $response_data ? $response_data : array();
    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function roomNoImport(Request $request){
    	if (!$request->isMethod('POST')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}

    	$request_data = $request->input();
        if (!isset($request_data['file_path']) || !$request_data['file_path']) {
            return response()->json(['code' => 0, 'message' => '导入文件路径为必填项']);
        }
    	$file_path = $request_data['file_path'];

    	if (is_file(app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Aliyun-oss'.DIRECTORY_SEPARATOR.'autoload.php')) {
            require_once app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Aliyun-oss'.DIRECTORY_SEPARATOR.'autoload.php';
        }

        try {
	        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
            $accessKeyId = "LTAIKSeZmyYSdn4Z";
            $accessKeySecret = "k33JSmnxYYybzVMTzrubP3sJIBOKc8";
            // Endpoint以杭州为例，其它Region请按实际情况填写。
            $endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
            // 存储空间名称
            $bucket= "rcfwlm-data";
	        // object 表示您在下载文件时需要指定的文件名称，如abc/efg/123.jpg。
			$object = substr($file_path, 48);
	    	$file_array = explode('/', $file_path);
			// 指定文件下载路径。
			$localfile = "../storage/". $file_array[count($file_array) - 1];
			$options = array(
		        OssClient::OSS_FILE_DOWNLOAD => $localfile
		    );
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->getObject($bucket, $object, $options);
        } catch (OssException $e) {
        	echo $e->getMessage();
            return response()->json(['code' => 0, 'message' => '获取导入文件失败']);
        }
	    $data = Excel::toArray(new RoomNoImport, $localfile);
	    $response_data = [];
	    if (isset($data[0])) {
		    foreach ($data[0] as $key => $value) {
		    	if ($key > 0) {
		            DB::beginTransaction();
		            try {
			    		$community_id = 0;
			    		$building_id = 0;
			    		$house_type_id = 0;
			    		// 查询小区是否存在
			    		$community = DB::table('community')->where('community_name', trim($value[0]))->where('status', '1')->first();
			    		if (!$community) {
			    			$community_id = DB::table('community')->insertGetId(array('community_name' => trim($value[0]), 'province' => trim($value[11]), 'city' => trim($value[12]), 'area' => trim($value[13]), 'address' => trim($value[14])));
			    			if (!$community_id) {
		                    	DB::rollBack();
		                    	continue;
			    			}
			    		} else {
			    			$community_id = $community->community_id;
			    		}
			    		// 查询楼栋是否存在
			    		$building = DB::table('building')->where('community_id', $community_id)->where('buildings', trim($value[1]))->where('unit', trim($value[2]))->where('status', '1')->first();
			    		if (!$building) {
		                	$building_id = DB::table('building')->insertGetId(array('community_id' => $community_id, 'buildings' => trim($value[1]), 'unit' => trim($value[2])));
			    			if (!$building_id) {
		                    	DB::rollBack();
		                    	continue;
			    			}
			    		} else {
			    			$building_id = $building->building_id;
			    		}
			    		// 查询户型是否存在
			    		$house_type = DB::table('house_type')->where('community_id', $community_id)->where('house_type_name', trim($value[6]))->where('status', '1')->first();
			    		if (!$house_type) {
		                	$house_type_id = DB::table('house_type')->insertGetId(array('community_id' => $community_id, 'house_type_name' => trim($value[6]), 'rooms' => trim($value[7]), 'halls' => trim($value[8]), 'guards' => trim($value[9]), 'construction_area' => trim($value[10])));
			    			if (!$house_type_id) {
		                    	DB::rollBack();
		                    	continue;
			    			}
			    		} else {
			    			$house_type_id = $house_type->house_type_id;
			    		}
			    		
			    		// 查询房号是否存在
			    		$room_no = DB::table('room_no')->where('community_id', $community_id)->where('building_id', $building_id)->where('house_type_id', $house_type_id)->where('units', trim($value[3]))->where('floors', trim($value[4]))->where('room_no', trim($value[5]))->where('status', '1')->first();
			    		if (!$room_no) {
		                	if (!DB::table('room_no')->insertGetId(array('community_id' => $community_id, 'building_id' => $building_id, 'house_type_id' => $house_type_id, 'units' => trim($value[3]), 'floors' => trim($value[4]), 'room_no' => trim($value[5])))) {
		                    	DB::rollBack();
		                	}
			    		}
			    		
			    		DB::commit();
		            }  catch (Exception $e) {
		                DB::rollBack();
		            }
		    	}
		    }
	    }
	    
        // 删除下载的EXCEL文件
        unlink($localfile);
        return response()->json(['code' => 1, 'message' => '导入成功']);
	}

    public function unitList(Request $request) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}
    	$request_data = $request->input();
        if (!isset($request_data['building_id']) || !$request_data['building_id']) {
            return response()->json(['code' => 0, 'message' => '楼栋ID为必填项']);
        }
    	$response_data = DB::table('room_no')->where('building_id', $request_data['building_id'])->where('status', '1')->distinct('units')->get('units');
    	$response_data = $response_data ? $response_data : array();
    	return response()->json(['code' => 1, 'data' => $response_data]);
    }

    public function floorList(Request $request) {
    	if (!$request->isMethod('GET')) {
	        return response()->json(['code' => 0, 'message' => '提交方式错误']);
    	}
    	$request_data = $request->input();
        if (!isset($request_data['building_id']) || !$request_data['building_id']) {
            return response()->json(['code' => 0, 'message' => '楼栋ID为必填项']);
        }
        if (!isset($request_data['units']) || !$request_data['units']) {
            return response()->json(['code' => 0, 'message' => '单元为必填项']);
        }
    	$response_data = DB::table('room_no')->where('units', $request_data['units'])->where('building_id', $request_data['building_id'])->where('status', '1')->distinct('floors')->get('floors');
    	$response_data = $response_data ? $response_data : array();
    	return response()->json(['code' => 1, 'data' => $response_data]);
    }
}
