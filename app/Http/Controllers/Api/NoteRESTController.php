<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-25
 * Time: 14:01
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class NoteRESTController extends Controller
{
    protected $note_config;


    //在类初始化方法中，引入相关类库
    public function __construct(){
        $this->note_config = [
            //主帐号,对应开官网发者主账号下的 ACCOUNT SID
            'accountSid' => '8aaf07085a362006015ae57dfde052cf',

            //主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
            'accountToken' =>'31e07a37e0984f578a2e1440174c8608',

            //应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
            //在开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID
            'appId' => '8aaf07085a362006015ae57dfe0552d0' ,

            //沙盒环境（用于应用开发调试）：sandboxapp.cloopen.com
            //生产环境（用户应用上线使用）：app.cloopen.com
            'serverIP' => 'app.cloopen.com',

            //请求端口，生产环境和沙盒环境一致
            'serverPort'=> '8883',

            //REST版本号，在官网文档REST介绍中获得。
            'softVersion'=> '2013-12-26',
        ];
    }

    public function index($to_phones,$code,$num='168443')
    {
        $bool = $this->sendTemplateSMS($to_phones,$code,$num);
    }

    public function sendTemplateSMS($to,$datas,$tempId)
    {
        include_once(app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'CCP_REST'.DIRECTORY_SEPARATOR.'CCPRestSDK.php');

        //主帐号,对应开官网发者主账号下的 ACCOUNT SID
        $accountSid = $this->note_config['accountSid'];

        //主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
        $accountToken = $this->note_config['accountToken'];

        //应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
        ////在开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID
        $appId = $this->note_config['appId'];

        //请求地址
        //沙盒环境（用于应用开发调试）：sandboxapp.cloopen.com
        //生产环境（用户应用上线使用）：app.cloopen.com
        $serverIP = $this->note_config['serverIP'];

        //请求端口，生产环境和沙盒环境一致
        $serverPort = $this->note_config['serverPort'];

        //REST版本号，在官网文档REST介绍中获得。
        $softVersion = $this->note_config['softVersion'];

        // 初始化REST SDK
        //global $accountSid,$accountToken,$appId,$serverIP,$serverPort,$softVersion;
        $rest = new \REST($serverIP,$serverPort,$softVersion);
        $rest->setAccount($accountSid,$accountToken);
        $rest->setAppId($appId);

        $result = $rest->sendTemplateSMS($to,$datas,$tempId);
        if($result == NULL ) {
            return false;
        }
        if($result->statusCode!=0) {
            $log = [
                'to_mobile' => $to,
                'self_check' => 'PASS',
                'return_code' => (string)$result->statusCode,
                'return_desc' => '发送失败',
                'return_status' => 'FAIL',
                'send_time' => (string)$result->statusMsg,
                'time' => time(),
            ];
            $this->log_result('FAIL.log', json_encode($log));
            return false;

            //TODO 添加错误处理逻辑
        }else{
            // 获取返回信息
            $smsmessage = $result->TemplateSMS;
            $log = [
                'to_mobile' => $to,
                'self_check' => 'PASS',
                'return_code' => (string)$result->statusCode,
                'return_desc' => '发送成功',
                'return_status' => 'SUCCESS',
                'send_time' => (string)$smsmessage->dateCreated,
                'time' => time(),
            ];
            $this->log_result('SUCCESS.log', json_encode($log));
            //TODO 添加成功处理逻辑
            return true;
        }
    }

    //日志
    private function log_result($file, $word)
    {
        $log_name = storage_path('logs/note/'. date("Y-m-d") . $file);
        $time = date("Y-m-d:H:i:s");
        file_put_contents($log_name, "【短信发送通知】" . $time . ":\n" . $word . "\n", FILE_APPEND);
    }
}