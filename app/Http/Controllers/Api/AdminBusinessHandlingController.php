<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ReservationService;
use Illuminate\Http\Request;

class AdminBusinessHandlingController extends Controller
{
    protected $reservationMoel;
    public function __construct()
    {
        $this->reservationMoel = new ReservationService();
    }

    public function list(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);


        $data = $request->input();

        $list = $this->reservationMoel->getReservationServiceList($data);

        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$list]);
    }


    /**
     * 服务受理
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function accept(Request $request)
    {
        if(!$request->has('id')){
            return response()->json(['code'=>'0','message'=>'请选择服务项目']);
        }

        $data= $request->input();
        $fuwu = $this->reservationMoel->getReservation(['id'=>$data['id']],['id','uid','status']);
        if(empty($fuwu)){
            return response()->json(['code'=>'0','message'=>'请选择服务项目']);
        }

        if($fuwu->status != 1){
            return response()->json(['code'=>'0','message'=>'此项目您不能受理']);
        }

        $temp['sl_user'] = $data['user']['uid'];
        $temp['status'] = 2;
        $temp['sl_time'] = time();
        $temp['updtime'] = $temp['sl_time'];

        $bool = $this->reservationMoel->updReservation(['id'=>$data['id']],$temp);
        if($bool){
            $message = new MessageController();
            $message->addMessage($fuwu->uid,'7','服务项目预约申请已受理，待受理完成',['is_status'=>'2']);
            $message->addMessage($fuwu->uid,'8','您预约的服务项目已受理');
            return response()->json(['code'=>'1','message'=>'受理成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'受理失败']);
        }
    }

    /**
     * 更新处理进度
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProcess(Request $request){
        if(!$request->has('id')){
            return response()->json(['code'=>'0','message'=>'请选择服务项目']);
        }

        $data= $request->input();
        $fuwu = $this->reservationMoel->getReservation(['id'=>$data['id']],['id','uid','status']);
        if(empty($fuwu)){
            return response()->json(['code'=>'0','message'=>'请选择服务项目']);
        }

        if($fuwu->status != 2){
            return response()->json(['code'=>'0','message'=>'您不能更新服务进度']);
        }

        $temp['process'] = $request->input('process');

        $bool = $this->reservationMoel->updReservation(['id'=>$data['id']],$temp);
        if($bool){
            $message = new MessageController();
            $message->addMessage($fuwu->uid,'8',$temp['process']);
            return response()->json(['code'=>'1','message'=>'更新服务进度成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'更新服务进度失败']);
        }
    }

    /**
     * 受理完成
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function achieveAccept(Request $request)
    {
        if(!$request->has('id')){
            return response()->json(['code'=>'0','message'=>'请选择服务项目']);
        }

        $data= $request->input();
        $fuwu = $this->reservationMoel->getReservation(['id'=>$data['id']],['id','uid','status']);
        if(empty($fuwu)){
            return response()->json(['code'=>'0','message'=>'请选择服务项目']);
        }

        if($fuwu->status != 2){
            return response()->json(['code'=>'0','message'=>'此项目您不能完成受理']);
        }

        $temp['wc_user'] = $data['user']['uid'];
        $temp['status'] = 3;
        $temp['wc_time'] = time();
        $temp['updtime'] = $temp['wc_time'];
        $temp['process'] = $data['process'];

        $bool = $this->reservationMoel->updReservation(['id'=>$data['id']],$temp);
        if($bool){
            $message = new MessageController();
            $message->addMessage($fuwu->uid,'8','您预约的服务项目已受理完成');
            return response()->json(['code'=>'1','message'=>'完成受理成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'完成受理失败']);
        }
    }

    public function getUserServiceList(Request $request)
    {
        $data = $request->input();
        if(empty($data['uid'])){
            return response()->json(['code'=>'0','message'=>'请选择用户']);
        }

        $list = $this->reservationMoel->getUserServiceList(['uid'=>$data['uid']]);
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$list]);
    }
}
