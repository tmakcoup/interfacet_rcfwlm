<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class AlibabaCloudController extends Controller
{
    public function client($data)
    {
        if (is_file(app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Aliyun-dysms'.DIRECTORY_SEPARATOR.'api_demo'.DIRECTORY_SEPARATOR.'SmsDemo.php')) {
            require_once app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Aliyun-dysms'.DIRECTORY_SEPARATOR.'api_demo'.DIRECTORY_SEPARATOR.'SmsDemo.php';
        }

        $alySms = new \SmsDemo();
        $result = $alySms->sendSms($data);
        if($result->Code == 'OK'){
            return ['code'=>'1','message'=>'短信发送成功'];
        }else{
            return ['code'=>'0','message'=>$result->Message];
        }
    }
}
