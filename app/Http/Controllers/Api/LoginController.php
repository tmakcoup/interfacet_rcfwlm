<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-24
 * Time: 16:13
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Member;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function adminLanding(Request $request)
    {
        $data = $request->input();
        $validator = \Validator::make($data,[
            'mobile'=>'required',
            'password'=>'required',
        ],[
            'required'=>':attribute为必填项',
        ],[
            'mobile'=>'账号',
            'password'=>'密码',
        ]);


        if($validator->fails()){
            $msg['code'] = '0';
            $msg['message'] = $validator->errors()->first();
            return response()->json($msg);
        }

        $temp = [
            'mobile' => $data['mobile'],
            'password' => $data['password'],
            'status' => '1',
        ];

        $customClaims = ['foo' => 'admin'];
        $token = $this->guard('admin')->claims($customClaims)->attempt($temp);
        if(empty($token)){
            return response()->json(['code'=>'0','message'=>'密码错误']);
        }


        $result = $this->respondWithToken($token,'admin');
        return response()->json($result);
    }

    /**
     * 移动端登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiLanding(Request $request)
    {
        $data = $request->input();
        if(empty($data['mobile'])){
            return response()->json(['code'=>'0','message'=>'手机号不能为空']);
        }else{
            if(!check_mobile($data['mobile'])){
                return response()->json(['code'=>'0','message'=>'请输入正确的手机号']);
            }
        }

        if(empty($data['code'])){
            return response()->json(['code'=>'0','message'=>'验证码不能为空']);
        }

        $code = new NoteCodeController();
        $bool = $code->verifyCode($data);
        if($bool['code'] == 0){
            return response()->json($bool);
        }

        $memberModel = new Member();
        $member = $memberModel->getMember(['mobile'=>$data['mobile']],['id','mobile','openid']);
        if(empty($member)){
            $temp['mobile'] = $data['mobile'];
            $bool = $memberModel->addMember($temp);
            if($bool['code'] == 0){
                return response()->json(['code'=>'0','message'=>'登录失败']);
            }

            $member = $memberModel->getMember(['mobile'=>$data['mobile']],['id','mobile','openid']);
        }

        $customClaims = ['foo' => 'member'];
        $token = \Tymon\JWTAuth\Facades\JWTAuth::claims($customClaims)->fromUser($member);


        $result = $this->respondWithToken($token,'member');
        return response()->json($result);
    }



    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return array
     */
    public function respondWithToken($token,$name)
    {
        return [
            'code'=>'1',
            'message'=>'登陆成功',
            'data'=>[
                'type'=>'1',
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth($name)->factory()->getTTL() * 600
            ]
        ];
    }


    /**
     * 自定义认证驱动
     *
     */
    protected function guard($name)
    {
        return auth()->guard($name);
    }


    /**
     * 密码找回
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrievePassword(Request $request)
    {
        $data = $request->input();
        if(empty($data['mobile'])){
            return response()->json(['code'=>'0','message'=>'电话号码不能为空']);
        }

        if(empty($data['code'])){
            return response()->json(['code'=>'0','message'=>'验证码不能为空']);
        }

        if(empty($data['password'])){
            return response()->json(['code'=>'0','message'=>'密码不能为空']);
        }

        $adminModel = new Admin();
        $admin = $adminModel->getAdmin(['mobile'=>$data['mobile']],['id','mobile']);
        if(empty($admin)){
            return response()->json(['code'=>'0','message'=>'账号不存在']);
        }

        $code = new NoteCodeController();
        $bool = $code->verifyCode(['code'=>$data['code'],'mobile'=>$admin->mobile]);
        if($bool['code'] == 0){
            return response()->json($bool);
        }

        $temp['password'] = bcrypt($data['password']);
        if($adminModel->where('id',$admin->id)->update($temp)){
            return response()->json(['code'=>'1','message'=>'找回成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'密码找回失败']);
        }
    }

}
