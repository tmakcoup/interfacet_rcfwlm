<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-23
 * Time: 15:11
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->input();

        $message = new MessageController();
        $message->addMessage('0','2','张三通过了初审，待复审',['is_check'=>'2']);
    }
}
