<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MemberTalents;
use Illuminate\Http\Request;

class TalentsInfoController extends Controller
{
    public function addTalents(Request $request)
    {
        $data = $request->input();
        $bool_data = $this->getValidator($data);
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }

        $memberTalentsModel = new MemberTalents();
        $talents = $memberTalentsModel->getTalents(['uid'=>$data['user']['uid']]);
        $bool_data['data']['user_id'] = 0;
        if(empty($talents)){
            $bool = $memberTalentsModel->addTalents($bool_data['data']);
            if($bool['code'] == 1){
                $message = new MessageController();
                $message->addMessage($data['user']['uid'],'9','会员人才信息注册申请待审核',['is_check'=>'2']);
            }
            return response()->json($bool);
        }else{
            if(!in_array($talents->is_check,['3','4'])){
                return response()->json(['code'=>'0','message'=>'您不能修改']);
            }elseif ($talents->is_check == 4 && $talents->freeze_time > time()){
                return response()->json(['code'=>'0','message'=>'冻结时间未到，您不能修改']);
            }
            $bool = $memberTalentsModel->addTalents($bool_data['data']);
            if($bool['code'] == 1){
                $message = new MessageController();
                $message->addMessage($data['user']['uid'],'9','会员人才信息注册申请待审核',['is_check'=>'2']);
            }
            return response()->json($bool);
        }

    }


    /**
     * 验证
     * @param $data
     * @return array
     */
    public function getValidator($data,$type=0)
    {
        $validator = \Validator::make($data,[
            'enterprise_name'=>'required',
            'area_range'=>'required',
            'nature_unit'=>'required',
            'firm_name'=>'required',
            'talents_type'=>'required',
            'name'=>'required',
            'mobile'=>'required',
            'identity_num'=>'required',
            'mingzu'=>'required',
            'birthplace'=>'required',
            //'social_security_address'=>'required',
            //'come_district_time'=>'required',
            'xueli'=>'required|integer',
            'xuewei'=>'required|integer',
            'technical_level'=>'required',
            'duty'=>'required',
//            'jz_address'=>'required',
//            'jz_area_range'=>'required',
            //'personal_experience'=>'required',
            'avatar'=>'required',
        ],[
            'required'=>':attribute为必填项',
            'max'=>':attribute长度不符合要求',
            'integer'=>':attribute必须为数字',
        ],[
            'enterprise_name'=>'企业名称',
            'area_range'=>'单位区域',
            'nature_unit'=>'单位性质',
            'firm_name'=>'行业名称',
            'talents_type'=>'人才类型',
            'name'=>'姓名',
            'mobile'=>'电话',
            'identity_num'=>'身份证号码',
            'mingzu'=>'民族',
            'birthplace'=>'籍贯',
            //'social_security_address'=>'参保地址',
            //'come_district_time'=>'来区时间',
            'xueli'=>'学历',
            'xuewei'=>'学位',
            'technical_level'=>'职称',
            'duty'=>'职务',
//            'jz_address'=>'居住地址',
//            'jz_area_range'=>'居住区域范围',
            //'personal_experience'=>'个人经历',
            'avatar'=>'个人头像',
        ]);


        //验证失败，并返回第一个报错
        if($validator->fails()) {
            return ['code' => 0, 'message' => $validator->errors()->first()];
        }

        //手机号验证
        if(!check_mobile($data['mobile'])){
            return ['code'=>'0','message'=>'请输入正确的手机号'];
        }

        if (is_file(app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Idcard'.DIRECTORY_SEPARATOR.'Idcard.php')) {
            require_once app_path().DIRECTORY_SEPARATOR.'Libraries'.DIRECTORY_SEPARATOR.'Idcard'.DIRECTORY_SEPARATOR.'Idcard.php';
        }

        $Idcard = new \Idcard();
        if(!$Idcard->isChinaIDCard($data['identity_num'])){
            return ['code'=>'0','message'=>"身份证号码错误：{$data['identity_num']}"];
        }

        $data['birthday'] = $Idcard->birthday;
        $sex = $Idcard->getChinaIDCardSex($data['identity_num']);
        if($sex == '男'){
            $data['sex'] = 1;
        }else{
            $data['sex'] = 2;
        }

        if($type == 0){
            if(empty($data['code'])){
                return ['code'=>'0','message'=>'验证码不能为空'];
            }

            $code = new NoteCodeController();
            $bool = $code->verifyCode(['code'=>$data['code'],'mobile'=>$data['mobile']]);
            if($bool['code'] == 0){
                return $bool;
            }
        }

        if(empty($data['social_security_address'])){
            $data['social_security_address'] = '';
        }

        if(empty($data['honor_grade'])){
            $data['honor_grade'] = '';
        }

        if(empty($data['honor_content'])){
            $data['honor_content'] = '';
        }


        if(empty($data['come_district_time'])){
            $data['come_district_time'] = '';
        }

        if(empty($data['personal_experience'])){
            $data['personal_experience'] = '';
        }

        if(empty($data['birthplace'])){
            $data['birthplace'] = '';
        }
        return ['code'=>1,'message'=>'验证成功','data'=>$data];
    }


    public function  reviewProgress(Request $request)
    {
        $user = $request->input('user');
        $memberTalentsModel = new MemberTalents();

        $dragon = $memberTalentsModel->getTalents(['uid'=>$user['uid']]);
        if(empty($dragon)){
            return response()->json(['code'=>'1','message'=>'您还未提交申请','data'=>['is_check'=>'0','remark'=>'','trial_time'=>'']]);
        }

        $temp['is_check'] = $dragon->is_check;
        $temp['remark'] = $dragon->remark;
        if(empty($dragon->trial_time)){
            $temp['trial_time'] = '';
        }else{
            $temp['trial_time'] = date('Y/m/d H:i:s',$dragon->trial_time);
        }


        if(!empty($dragon->freeze_time)){
            $temp['freeze_time'] = date('Y/m/d',$dragon->freeze_time);
        }else{
            $temp['freeze_time']  = '';
        }

        $temp['is_freeze'] = $dragon->is_freeze;

        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$temp]);
    }


    public function getDetails(Request $request)
    {
        $user = $request->input('user');
        $memberTalentsModel = new MemberTalents();
        $member = $memberTalentsModel->getDetails($user['uid']);
        return response()->json(['code' => '1', 'message' => '获取成功', 'data' => $member]);
    }

}
