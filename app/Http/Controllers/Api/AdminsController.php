<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-23
 * Time: 15:16
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
    protected $adminModel;

    public function __construct()
    {
        $this->adminModel = new Admin();
    }

    public function list(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);

        $admin = $this->adminModel->getAdminsList($request->input());
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$admin]);
    }

    public function removeAdmins(Request $request)
    {
        if(!$request->has('id')){
            return response()->json(['code'=>'0','message'=>'请上传用户id']);
        }

        $id = $request->input('id');
        $admin = $this->adminModel->where('id',$id)->where('status',1)->first();
        if(empty($admin)){
            return response()->json(['code'=>'0','message'=>'请选择用户']);
        }elseif ($admin->id == 1){
            return response()->json(['code'=>'0','message'=>'超级管理员不能删除']);
        }

        if($this->adminModel->where('id',$id)->update(['status'=>'0'])){
            return response()->json(['code'=>'1','message'=>'删除成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'删除失败']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function details(Request $request)
    {

        $bool_admin = $this->boolAdmin($request->input(),0);
        if($bool_admin['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_admin['message'];
            return response()->json($result);
        }

        return response()->json([
            'code'=>'1',
            'message'=>'获取成功',
            'data'=>$bool_admin['data'],
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAdmins(Request $request)
    {
        $bool_data = $this->getValidator($request->input());
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }

        $bool = $this->adminModel->addAdmin($request->input());
        return response()->json($bool);
    }

    /**
     * @param Request $request
     */
    public function updAdmins(Request $request)
    {
        $data = $request->input();
        if(empty($data['password'])){
            $data['password'] = '-----';
        }
        $bool_data = $this->getValidator($data);
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }

        $bool_admin = $this->boolAdmin($data,0);
        if($bool_admin['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_admin['message'];
            return response()->json($result);
        }


        $where['id'] = $data['id'];
        unset($data['user']);
        if($bool_admin['data']->mobile == $data['mobile']){
            $bool_upd = $this->adminModel->updAdmin($where,$data);
        }else{
            $bool_admin1 = $this->boolAdmin(['mobile'=>$data['mobile']],1);

            if($bool_admin1['code'] == 1){
                return response()->json(['code'=>'0','message'=>'此账号已添加']);
            }

            $bool_upd = $this->adminModel->updAdmin($where,$data);
        }

        return response()->json($bool_upd);
    }


    /**
     * 用户获取
     * @param $data
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function boolAdmin($data,$type)
    {
        if($type == 0){
            if(empty($data['id'])){
                $data['id'] = $data['user']['uid'];
            }
            $where = ['id'=>$data['id']];
        }else{
            $where = ['mobile'=>$data['mobile']];
        }


        $admin = $this->adminModel->getAdmin($where,['id','name','mobile']);
        if(empty($admin)){
            return [
                'code'=>'0',
                'message'=>'请选择用户',
            ];
        }

        return ['code'=>'1','message'=>'获取成功','data'=>$admin];
    }


    /**
     * 验证
     * @param $data
     * @return array
     */
    public function getValidator($data)
    {
        $validator = \Validator::make($data,[
            'mobile'=>'required|integer',
            'name'=>'required|max:10',
            'password'=>'required',
            'role_id'=>'required|integer',
        ],[
            'required'=>':attribute为必填项',
            'max'=>':attribute长度不符合要求',
            'integer'=>':attribute必须为数字',
        ],[
            'mobile'=>'账号',
            'name'=>'姓名',
            'password'=>'密码',
            'role_id'=>'角色',
        ]);


        //验证失败，并返回第一个报错
        if($validator->fails()) {
            return ['code' => 0, 'message' => $validator->errors()->first()];
        }

        if(!check_mobile($data['mobile'])){
            return response()->json(['code'=>'0','message'=>'请输入正确的手机号']);
        }

        return ['code'=>1,'message'=>'验证成功'];
    }

    /**
     * 密码修改
     */
    public function changePassword(Request $request)
    {
        $data = $request->input();
        if(empty($data['code'])){
            return response()->json(['code'=>'0','message'=>'验证码不能为空']);
        }

        if(empty($data['password'])){
            return response()->json(['code'=>'0','message'=>'密码不能为空']);
        }
        $user = $request->input('user');
        $admin = $this->adminModel->getAdmin(['id'=>$user['uid']],['mobile']);

        $code = new NoteCodeController();
        $bool = $code->verifyCode(['code'=>$data['code'],'mobile'=>$admin->mobile]);
        if($bool['code'] == 0){
            return response()->json($bool);
        }

        $temp['password'] = bcrypt($data['password']);
        if($this->adminModel->where('id',$user['uid'])->update($temp)){
            return response()->json(['code'=>'1','message'=>'修改成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'修改失败']);
        }
    }
}
