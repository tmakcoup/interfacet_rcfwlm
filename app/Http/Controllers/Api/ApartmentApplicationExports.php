<?php

namespace App\Http\Controllers\Api;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class ApartmentApplicationExports implements FromCollection, WithHeadings
{
    public function collection()
    {
        return DB::table('apartment_application')->where('status', 1)->get([
        	'unit_name', 
        	'corporate_compliance', 
        	'organization_code', 
        	'unit_nature', 
        	'field',
        	'contact_name',
        	'contact_phone',
        	'contact_address',
        	'talent_category',
        	'real_name',
        	'phone',
        	'sex',
        	DB::raw("DATE_FORMAT(birthday, '%Y-%m-%d')"),
        	DB::raw("CONCAT(idcard,' ')"),
        	'nationality',
        	'birthplace',
        	'social_security',
        	DB::raw("DATE_FORMAT(come_work_time, '%Y-%m-%d')"),
        	'education',
        	'degree',
        	'job_title',
        	'position',
        	DB::raw("DATE_FORMAT(work_time, '%Y-%m-%d')"),
        	'house_situation',
        	'address',
        	DB::raw("DATE_FORMAT(lease_period_start_time, '%Y-%m-%d')"),
        	DB::raw("DATE_FORMAT(lease_period_end_time, '%Y-%m-%d')"),
        	'study_work_experience',
        	'marriage_childbirth',
        	'spouse_name',
        	DB::raw("CONCAT(spouse_idcard,' ')"),
        	'spouse_contact',
        	'spouse_work_unit',
        	'spouse_house_situation',
        	'unit_name_range', 
        	'honors',
        	'honor_level',
        	DB::raw("CASE approval_status WHEN '0' THEN '待初审' WHEN '1' THEN '待复审' WHEN '2' THEN '初审不通过' WHEN '3' THEN '初审驳回' WHEN '4' THEN '审批通过' WHEN '5' THEN '审批不通过' WHEN '6' THEN '审批驳回' END"),
            'add_time',
            'approval_time'
        ]);
    }


    public function headings(): array
    {
        return [
        	'单位名称',
            '法人代表',
            '组织机构代码',
            '单位性质',
            '所属领域',
            '联系人',
            '联系电话',
            '联系地址',
            '人才类别',
            '真实姓名',
            '联系电话',
            '性别',
            '出生日期',
            '身份证',
            '民族',
            '籍贯',
            '社保参保地',
            '来区工作时间',
            '学历',
            '学位',
            '职称',
            '职务',
            '任职时间',
            '住房情况',
            '详细地址',
            '申请租期开始时间',
            '申请租期结束时间',
            '学习与工作经历',
            '婚育情况',
            '配偶名称',
            '配偶身份证',
            '配偶联系方式',
            '配偶工作单位',
            '配偶住房情况',
        	'单位区域',
        	'荣誉',
        	'荣誉级别',
            '状态',
            '添加时间',
            '审核时间',
        ];
    }
}