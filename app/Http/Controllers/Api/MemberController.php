<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{

    public function updMobile(Request $request,Member $member)
    {
        $data = $request->input();
        $bool = $member->updMobile($data);
        return response()->json($bool);
    }
}
