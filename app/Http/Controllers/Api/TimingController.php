<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MemberDragon;
use App\Models\MemberTalents;
use DB;

class TimingController extends Controller
{
    public function message()
    {
        $memberDragon = new MemberDragon();
        $time = strtotime("+1 month");
        $list = $memberDragon
            ->where('is_check','1')
            ->where('is_dated','0')
            ->where('over_time','<',$time)
            ->get(['uid']);

        $message = new MessageController();
        foreach ($list as $key=>$value){
            $bool = $memberDragon->where('uid',$value->uid)->where('is_dated','0')->update(['is_dated'=>1]);
            if($bool){
                $message->addMessage($value->uid,'11','您的龙卡还有1个月到期，过期将不能再预约服务，请及时预约',[]);
            }
        }

        $room_no_list = DB::table('room_no')->where('apartment_application_id', '>', 0)->where(DB::raw('TIMESTAMPDIFF(MONTH,NOW(),lease_period_end_time)'), 1)->get();
        foreach ($room_no_list as $key => $value) {
            $apartment_application = DB::table('apartment_application')->where('apartment_application_id', $value->apartment_application_id)->where('is_send_message', 0)->first();
            if ($apartment_application) {
                if(DB::table('apartment_application')->where('apartment_application_id', $value->apartment_application_id)->update(array('is_send_message' => 1))) {
                    $message->addMessage($apartment_application->user_id,'70','您的房源还有一个月到期，请及时申请续租',[]);
                }
            }
        }

        $this->applyUnfreeze();
    }

    /**
     * 申请冻结，解封
     */
    public function applyUnfreeze()
    {
        //龙卡
        $memberDragon = new MemberDragon();
        $time = time();
        $list = $memberDragon
            ->where('is_check','5')
            ->where('is_freeze','1')
            ->where('freeze_time','<',$time)
            ->get(['uid']);

        foreach ($list as $key=>$value){
            $memberDragon->where('uid',$value->uid)->where('is_freeze','1')->update(['is_freeze'=>0]);
        }

        //人才
        $memberTalents = new MemberTalents();
        $talents = $memberTalents
            ->where('is_check','4')
            ->where('is_freeze','1')
            ->where('freeze_time','<',$time)
            ->get(['uid']);

        foreach ($talents as $k=>$v){
            $memberTalents->where('uid',$v->uid)->where('is_freeze','1')->update(['is_freeze'=>0]);
        }
    }
}
