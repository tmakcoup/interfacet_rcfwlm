<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-05-07
 * Time: 10:07
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\MemberDragon;
use Illuminate\Http\Request;

class AdminManageCardController extends Controller
{

    /**
     * 卡片生成
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCard(Request $request)
    {
        if(!$request->has('uid')){
            return response()->json(['code'=>'0','message'=>'请选择用户']);
        }

        $uid = $request->input('uid');
        $memberDragonModel = new MemberDragon();
        $dragon = $memberDragonModel->getDragon(['uid'=>$uid],['uid','is_check','card_num','qrcode','onset_time','over_time','status']);
        if(empty($dragon)){
            return response()->json(['code'=>'0','message'=>'请选择用户']);
        }elseif ($dragon->is_check != 1){
            return response()->json(['code'=>'0','message'=>'此用户暂未审核通过不能发放']);
        }elseif ($dragon->status != 1){
            $data = $this->getDragon($uid);
            $data['onset_time'] = date('Y/m/d',$dragon->onset_time);
            $data['over_time'] = date('Y/m/d',$dragon->over_time);
            $data['card_num'] = $dragon->card_num;
            $data['qrcode'] = $dragon->qrcode;

            return response()->json(['code'=>'1','message'=>'生成成功','data'=>$data]);
        }else{
            $bool = $this->setDragioQrcode($uid,$memberDragonModel);
        }

        if($bool['code'] == 0){
            return response()->json(['code'=>'0','message'=>$bool['message']]);
        }
        $data = $this->getDragon($uid);
        $data['card_num'] = $dragon->card_num;
        $data['qrcode'] = $bool['qrcode'];
        $data['onset_time'] = date('Y/m/d',$dragon->onset_time);
        $data['over_time'] = date('Y/m/d',$dragon->over_time);

        $temp['status'] = 2;
        $temp['updtime'] = time();
        $memberDragonModel->updDragon($uid,$temp);
        return response()->json(['code'=>'1','message'=>'生成成功','data'=>$data]);
    }

    /**
     * 龙卡二维码
     * @param $uid
     * @param $memberDragonModel
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function setDragioQrcode($uid,$memberDragonModel)
    {
        $qr_code = new QrcodeController();
        $aly = $qr_code->makeQrcodeImg($uid);
        if($aly['code'] == 0){
            return ['code'=>'0','message'=>$aly['message']];
        }


        $temp['qrcode'] = $aly['path'];
        $temp['salt'] = $aly['salt'];
        $temp['status'] = 1;
        $temp['hairpin_time'] = time();
        $temp['updtime'] = $temp['hairpin_time'];
        $bool = $memberDragonModel->updDragon($uid,$temp);
        if($bool['code'] == 0){
            return ['code'=>'0','message'=>'生成失败'];
        }else{
            return ['code'=>'1','message'=>'生成成功','qrcode'=>$temp['qrcode']];
        }
    }

    /**
     * 卡片信息数据获取
     * @param $uid
     * @return mixed
     */
    public function getDragon($uid)
    {
        $memberModel = new Member();
        return $memberModel->getMemberDragon($uid);
    }


    /**
     * 卡片领取
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function drawCard(Request $request)
    {
        if(!$request->has('uid')){
            return response()->json(['code'=>'0','message'=>'请选择用户']);
        }

        $uid = $request->input('uid');
        $memberDragonModel = new MemberDragon();
        $dragon = $memberDragonModel->getDragon(['uid'=>$uid],['uid','is_check','card_num','qrcode','status']);
        if(empty($dragon)){
            return response()->json(['code'=>'0','message'=>'请选择用户']);
        }elseif ($dragon->is_check != 1){
            return response()->json(['code'=>'0','message'=>'此用户暂未审核通过不能领取']);
        }elseif ($dragon->status == 1) {
            return response()->json(['code' => '0', 'message' => '龙卡还未生成']);
        }elseif ($dragon->status == 3){
            return response()->json(['code' => '0', 'message' => '此卡已领取，请不要重复领取']);
        }

        $bool = $memberDragonModel->updDragon($uid,['status'=>3,'updtime'=>time()]);
        if($bool['code'] == 1){
            $message = new MessageController();
            $message->addMessage($uid,'5','您的卡片已领取，可预约服务');
            return response()->json(['code' => '1', 'message' => '领取成功']);
        }else{
            return response()->json(['code' => '0', 'message' => '领取失败']);
        }
    }
}
