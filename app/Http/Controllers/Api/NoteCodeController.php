<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-25
 * Time: 14:08
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Member;
use Illuminate\Http\Request;
use DB;

class NoteCodeController extends Controller
{
    public function getCode(Request $request)
    {
        $data = $request->input();
        $validator = $this->getValidator($data);
        if($validator['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $validator['message'];
            return response()->json($result);
        }


        $alibaba = new AlibabaCloudController();
        $phoneCode = (string)mt_rand(1000, 9999);
        $temp_sms['PhoneNumbers'] = $data['mobile'];
        $temp_sms['SignName'] = '人才服务发展研究中心';
        $temp_sms['TemplateCode'] = 'SMS_167964719';
        $temp_sms['TemplateParam'] = json_encode(['code'=>$phoneCode],JSON_UNESCAPED_UNICODE);
        $bool = $alibaba->client($temp_sms);
        if($bool['code'] == 1){
            $temp['mobile'] = $data['type'];
            $temp['mobile'] = $data['mobile'];
            $temp['type'] = $data['type'];
            $temp['code'] = $phoneCode;
            $temp['addtime'] = time();
            $temp['y_m_d'] = date('Y-m-d',$temp['addtime']);
            $temp['endtime'] = $temp['addtime'] + 900;
            if(DB::table('member_codelog')->insert($temp)){
                $result['code'] = '1';
                $result['message'] = '验证码发送成功';
                $result['mobile'] = $data['mobile'];
                return response()->json($result);
            }else{
                $result['code'] = '0';
                $result['message'] = '验证码发送失败';
                $result['mobile'] = $data['mobile'];
                return response()->json($result);
            }
        }else{
            $result['code'] = '0';
            $result['message'] = '验证码发送失败';
            $result['mobile'] = $data['mobile'];
            return response()->json($result);
        }
    }


    /**
     * 表单验证
     * @param $data
     * @return array
     */
    protected function getValidator($data)
    {
        if(empty($data['type'])){//1注册、2登录、3修改信息、4修改手机号
            return ['code'=>'0','message'=>'请上传验证码类型'];
        }

        if(empty($data['mobile'])){
            return ['code'=>'0','message'=>'请输入正确的手机号'];
        }else{
            if(!check_mobile($data['mobile'])){
                return ['code'=>'0','message'=>'请输入正确的手机号'];
            }
        }

        if($data['type']==4||$data['type']==1){
            $memberModel = new Member();
            $member = $memberModel->getMember(['mobile'=>$data['mobile']],['id','is_check']);
            if(!empty($member)){
                return ['code'=>'0','message'=>'此手机号已经注册了'];
            }
        }

        return ['code'=>'1','message'=>'验证通过'];
    }

    /**
     * 验证码验证
     * @param $data
     * @return array
     */
    public function verifyCode($data)
    {
        //查看验证码
        $code_info = DB::table('member_codelog')
            ->where('mobile',$data['mobile'])
            ->where('status',1)
            ->orderBy('id','desc')
            ->first();

        if(empty($code_info)){
            $result['code'] = '0';
            $result['message'] = '请先获取验证码';
            return $result;
        }

        if($code_info->endtime < time()){
            DB::table('member_codelog')->where('id',$code_info->id)->update(['status'=>2,'updtime'=>time()]);
            $result['code'] = '0';
            $result['message'] = '验证码过期，请重新获取';
            return $result;
        }

        if($code_info->code != $data['code']){
            $result['code'] = '0';
            $result['message'] = '请输入正确的验证码';
            return $result;
        }

        DB::table('member_codelog')->where('id',$code_info->id)->update(['status'=>0,'updtime'=>time()]);
        return ['code'=>1];
    }
}
