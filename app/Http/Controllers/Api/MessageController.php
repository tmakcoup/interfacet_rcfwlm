<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\PermissionRole;
use App\Models\RoleUser;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    private $message_config;
    private $messageModel;

    public function __construct()
    {
        $this->message_config = config('message');
        $this->messageModel = new Message();
    }

    /**
     * @param $uid  前台用户id
     * @param $message_id 消息类型id
     * @param $content 消息内容
     * @param array $args 参数数组
     * @return bool
     */
    public function addMessage($uid,$message_id,$content,$args=[])
    {
        if(empty($this->message_config[$message_id])){
            return false;
        }
        $temp['uid'] = $uid;
        $temp['type'] = $this->message_config[$message_id]['type'];
        $temp['title'] = $this->message_config[$message_id]['title'];
        $temp['message_id'] = $this->message_config[$message_id]['id'];
        $temp['url'] = $this->message_config[$message_id]['url'];
        $temp['mark'] = $this->message_config[$message_id]['mark'];
        $temp['regulation'] = $this->message_config[$message_id]['regulation'];
        $temp['content'] = $content;
        $temp['args'] = json_encode($args);
        $temp['addtime'] = time();
        return $this->messageModel->addMessage($temp);
    }

    public function apiList(Request $request)
    {
        if(!$request->has('page_size')){
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);
        }
        $user = $request->input('user');
        $info = $this->messageModel
            ->where('uid',$user['uid'])
            ->where('status','1')
            ->where('type','1')
            ->orderBy('id','desc')
            ->select(['id','title','content','message_id','addtime'])
            ->paginate($request->input('page_size'));
        foreach ($info as $key=>$value){
            $info[$key]->addtime = date('Y/m/d',$value->addtime);
        }

        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$info]);
    }

    public function apiMessageRead(Request $request)
    {
        $data = $request->input();
        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'消息id不能为空']);
        }

        $bool = $this->messageModel
            ->where('id',$data['id'])
            ->where('uid',$data['user']['uid'])
            ->update(['status'=>'2','updtime'=>time()]);

        if($bool){
            return response()->json(['code'=>'1','message'=>'操作成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'操作失败']);
        }
    }


    /**
     * 已读处理
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminMessageRead(Request $request)
    {
        $data = $request->input();
        if(empty($data['message_id'])){
            return response()->json(['code'=>'0','message'=>'消息类型不能为空']);
        }

        if(empty($data['ids'])){
            return response()->json(['code'=>'0','message'=>'消息id不能为空']);
        }

        $ids = explode(',',$data['ids']);
        $bool = $this->messageModel
            ->where('type','2')
            ->where('status','1')
            ->where('message_id',$data['message_id'])
            ->whereIn('id',$ids)
            ->update(['status'=>'2','updtime'=>time()]);

        if($bool){
            return response()->json(['code'=>'1','message'=>'操作成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'操作失败']);
        }
    }


    public function adminList(Request $request)
    {
        if(!$request->has('page_size')){
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);
        }
        $user = $request->input('user');
        $roleModel = new RoleUser();
        $role = $roleModel->getRoleUser(['user_id'=>$user['uid']]);
        if(empty($role)){
            return response()->json(['code'=>'1','message'=>'获取成功','data'=>[],'count'=>'0']);
        }

        $PermissionRole = new PermissionRole();
        $permission = $PermissionRole->where('role_id',$role->role_id)->get(['permission_id'])->toArray();
        if(empty($permission)){
            return response()->json(['code'=>'1','message'=>'获取成功','data'=>[],'count'=>'0']);
        }

        $permission_ids = array_reduce($permission, function ($result, $value) {
            return array_merge($result, [$value['permission_id']]);
        }, array());


        $count = $this->messageModel
            ->where('type','2')
            ->where('status','1')
            ->whereIn('regulation',$permission_ids)
            ->get(['id'])
            ->count();

        $info = $this->messageModel
            ->where('type','2')
            ->where('status','1')
            ->whereIn('regulation',$permission_ids)
            ->groupBy('message_id')
            ->select(['message_id'])
            ->paginate($request->input('page_size'));

        foreach ($info as $key=>$value){
            $info[$key]->title = $this->message_config[$value->message_id]['title'];
            $ids = $this->messageIds($value->message_id,$permission_ids);
            $info[$key]->count = count($ids);
            $info[$key]->ids = implode(',',$ids);
        }

        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$info,'count'=>$count]);
    }

    public function messageIds($message_id,$permission_ids)
    {
        $ids = $this->messageModel
            ->where('type','2')
            ->where('status','1')
            ->whereIn('regulation',$permission_ids)
            ->where('message_id',$message_id)
            ->get('id')->toArray();

        return array_reduce($ids, function ($result, $value) {
            return array_merge($result, [$value['id']]);
        }, array());
    }
}
