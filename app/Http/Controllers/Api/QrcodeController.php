<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-05-06
 * Time: 17:56
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrcodeController extends Controller
{
    /**
     * 龙卡二维码生成
     * @param $uid
     * @return array
     */
    public function makeQrcodeImg($uid)
    {
        $salt = get_salt();
        $temp = 'uid='.$uid.'&salt='.$salt.'&time='.time();
        $url = 'http://talentservice.rcfwlm.net/#/service_reservation?signature='.urlencode(base64_encode($temp));
        $path = 'api/qrcodes/dragon_card/'.time().'.png';
        QrCode::errorCorrection('H')->encoding('UTF-8')->format('png')->size(500)->generate($url,public_path($path));
        if(!file_exists($path)){
            return ['code'=>'0','message'=>'生成失败'];
        }

        $upload = new ImageController();
        $name = "fishery-shop/uploads/qrcode/".date('Y-m-d')."/".$uid.'.png';
        $aly = $upload->AliyunOss($name,$path,1);
        if($aly['info']['http_code'] != 200){
            return [
                'message'=>'生成失败',
                'code'=>'0',
            ];
        }


        $result['code'] = '1';
        $result['message'] = '生成成功';
        $result['path'] = $aly['oss-request-url'];
        $result['salt'] = $salt;
        return $result;

    }
}
