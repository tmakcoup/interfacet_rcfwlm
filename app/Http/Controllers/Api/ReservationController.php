<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MemberDragon;
use App\Models\ReservationService;
use App\Models\ServiceProject;
use Illuminate\Http\Request;

class ReservationController extends Controller
{

    protected $reservationModel;
    protected $memberDragonModel;

    public function __construct()
    {
        $this->reservationModel = new ReservationService();
        $this->memberDragonModel = new MemberDragon();
    }


    /**
     * 扫描判断
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(Request $request)
    {
        $data = $request->input();
        if(empty($data['signature'])){
            return response()->json(['code'=>'0','message'=>'扫码信息有误','data'=>['flag'=>'0']]);
        }

        $signature = return_par($data['signature']);
        if(empty($signature['uid']) || empty($signature['salt']) || empty($signature['time'])){
            return response()->json(['code'=>'0','message'=>'扫码信息有误','data'=>['flag'=>'0']]);
        }


        if($data['user']['uid'] != $signature['uid']){
            return response()->json(['code'=>'0','message'=>'请扫描自己的龙卡','data'=>['flag'=>'2']]);
        }


        $dragon = $this->memberDragonModel
            ->getDragon(['uid'=>$signature['uid'],'is_check'=>'1','salt'=>$signature['salt']],['uid','status']);
        if(empty($dragon)) {
            return response()->json(['code' => '0', 'message' => '请先申请龙卡', 'data' => ['flag' => '3']]);
        }elseif ($dragon->status != 3){
            return response()->json(['code' => '0', 'message' => '请先领取龙卡', 'data' => ['flag' => '4']]);
        }

        return response()->json(['code' => '1', 'message' => '获取成功', 'data' => ['flag' => '1']]);
    }


    public function list(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);
        $data = $request->input();
        $list = $this->reservationModel->getApiReservationServiceList(['uid'=>$data['user']['uid'],'page_size'=>$data['page_size']]);
        $count = $this->reservationModel->getCount(['uid'=>$data['user']['uid']]);
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$list,'count'=>$count]);
    }

    /**
     * 详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function details(Request $request)
    {
        $data = $request->input();
        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'请选择服务']);
        }
        $data = $this->reservationModel->getDetails(['uid'=>$data['user']['uid'],'id'=>$data['id']]);
        return response()->json($data);
    }

    public function cancel(Request $request)
    {
        $data = $request->input();
        if(empty($data['id'])){
            return response()->json(['code'=>'0','message'=>'请选择服务']);
        }
        $data = $this->reservationModel->disposeCancel(['uid'=>$data['user']['uid'],'id'=>$data['id']]);
        return response()->json($data);
    }

    /**
     * 获取项目
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItems()
    {
        $service = new ServiceProject();

        $info = $service->where('status','1')->get(['id','title','describe']);
        $temp = [];
        foreach ($info as $key=>$value){
            $temp[] = [
                'title' => $value->title,
                'describe' => nl2br($value->describe),
                'id' => $value->id,
            ];
        }

        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$temp]);
    }

    /**
     * 预约服务
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bookingService(Request $request)
    {
        $bool_data = $this->getValidator($request->input());
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }


        $bool = $this->reservationModel->addReservation($bool_data['data']);
        if($bool['code'] == 1){
            $message = new MessageController();
            $message->addMessage($bool_data['data']['uid'],'6','服务项目预约申请待受理',['is_status'=>'1']);
        }
        return response()->json($bool);
    }

    /**
     * 验证
     * @param $data
     * @return array
     */
    public function getValidator($data)
    {
        $validator = \Validator::make($data,[
            'item_id'=>'required|integer',
//            'onset_time'=>'required',
//            'over_time'=>'required',
//            'content'=>'required',
        ],[
            'required'=>':attribute为必填项',
            'max'=>':attribute长度不符合要求',
            'integer'=>':attribute必须为数字',
        ],[
            'item_id'=>'预约项目',
//            'onset_time'=>'预约开始时间',
//            'over_time'=>'预约结束时间',
//            'content'=>'服务需求',
        ]);


        //验证失败，并返回第一个报错
        if($validator->fails()) {
            return ['code' => 0, 'message' => $validator->errors()->first()];
        }

        $dragon = $this->memberDragonModel->getDragon(['uid'=>$data['user']['uid']],['uid','is_check','status','over_time']);
        if(empty($dragon)){
            return ['code'=>'0','message'=>'请申请龙卡'];
        }

        if($dragon->is_check != 1){
            return ['code'=>'0','message'=>'龙卡还未审核通过'];
        }


        if($dragon->status != 3){
            return ['code'=>'0','message'=>'请先领取龙卡'];
        }

        if($dragon->over_time < time()){
            return ['code'=>'0','message'=>'您的龙卡已过期，不能申请'];
        }


        $temp['item_id'] = $data['item_id'];
        $temp['content'] = $data['content'];
        $temp['onset_time'] = strtotime($data['onset_time']);
        $temp['over_time'] = strtotime($data['over_time']);
        $temp['uid'] = $data['user']['uid'];
        $temp['updtime'] = time();
        $temp['addtime'] = $temp['updtime'];
        $temp['status'] = 1;
        return ['code'=>1,'message'=>'验证成功','data'=>$temp];
    }

    public function isHairpin(Request $request)
    {
        $user = $request->input('user');
        $dragon = $this->memberDragonModel->getDragon(['is_check'=>'1','status'=>'3','uid'=>$user['uid']],['uid','card_num']);
        if(empty($dragon)){
            return response()->json(['code'=>'1','message'=>'获取成功','data'=>['type'=>'2']]);
        }else{
            return response()->json(['code'=>'1','message'=>'获取成功','data'=>['type'=>'1']]);
        }
    }
}
