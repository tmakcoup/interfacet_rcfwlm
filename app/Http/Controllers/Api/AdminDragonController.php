<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-05-05
 * Time: 09:26
 */
namespace App\Http\Controllers\Api;

use App\Exports\MemberExports;
use App\Http\Controllers\Controller;
use App\Imports\MemberDragonImports;
use App\Models\Member;
use App\Models\MemberBasic;
use App\Models\MemberBasicInfo;
use App\Models\MemberDragon;
use App\Models\MemberEnterprise;
use App\Models\ServiceProject;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class AdminDragonController extends Controller
{
    protected $memberModel;
    protected $memberDragonModel;

    public function __construct()
    {
        $this->memberModel = new Member();
        $this->memberDragonModel = new MemberDragon();
    }

    /**
     * 审核列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list1(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);

        if(!$request->has('is_check')){
            return response()->json(['code'=>'0','message'=>'请上传列表类型']);
        }
        $data = $request->input();
        $data['is_mark'] = 0;
        $member = $this->memberModel->getMemberListDragon($data);
        return response()->json(['code'=>'1','message'=>'请求成功','data'=>$member]);
    }

    /**
     * 生成龙卡
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list2(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);

        $data = $request->input();

        $data['is_check'] = 1;
        $data['is_mark'] = 4;

        $member = $this->memberModel->getMemberListDragon($data);
        return response()->json(['code'=>'1','message'=>'请求成功','data'=>$member]);
    }

    /**
     * 发卡管理
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list3(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);

        $data = $request->input();

        if(empty($data['is_mark'])){
            return response()->json(['code'=>'0','message'=>'请上传列表展示位置']);
        }

        if(!in_array($data['is_mark'],['1','2','3'])){
            return response()->json(['code'=>'0','message'=>'请上传列表展示位置']);
        }
        $data['is_check'] = 1;

        $member = $this->memberModel->getMemberListDragon($data);
        return response()->json(['code'=>'1','message'=>'请求成功','data'=>$member]);
    }


    public function list4(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);

        $data = $request->input();

        $data['is_check'] = 1;
        $data['is_mark'] = 3;

        $member = $this->memberModel->getMemberListDragon($data);
        return response()->json(['code'=>'1','message'=>'请求成功','data'=>$member]);
    }

    /**
     * 第一次审核
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function firstTrialDragon(Request $request)
    {
        $data = $request->input();
        $bool_data = $this->getValidator($data);
        if($bool_data['code'] == 0){
            return response()->json(['code'=>'0','message'=>$bool_data['message']]);
        }

        $boola = $this->memberDragonModel->updDragon($data['uid'],$bool_data['data']);
        $boolb = $this->memberModel->updMember(['id'=>$data['uid']],['is_check'=>$bool_data['data']['is_check']]);
        if($boola || $boolb){
            $message = new MessageController();
            if ($bool_data['data']['is_check'] == 3){
                $memberBasicModel = new MemberBasic();
                $basic = $memberBasicModel->getBasicKeys(['uid'=>$data['uid']],['name','uid']);
                $message->addMessage($data['uid'],'2',$basic->name.'初审通过，待复审',['is_check'=>'3']);//后端消息
                $message->addMessage($data['uid'],'3','您的人才龙卡申请已初审通过');//前端消息
            }elseif ($bool_data['data']['is_check'] == 4){
                $message->addMessage($data['uid'],'3','您的人才龙卡初审驳回');
            }elseif ($bool_data['data']['is_check'] == 5){
                $message->addMessage($data['uid'],'3','您的人才龙卡初审不通过');
            }
            return response()->json(['code'=>'1','message'=>'审核成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'审核失败']);
        }
    }


    /**第二次审核
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function secondTrialDragon(Request $request)
    {
        $data = $request->input();
        $bool_data = $this->getValidator($data);
        if($bool_data['code'] == 0){
            return response()->json(['code'=>'0','message'=>$bool_data['message']]);
        }

        $boola = $this->memberDragonModel->updDragon($data['uid'],$bool_data['data']);
        $boolb = $this->memberModel->updMember(['id'=>$data['uid']],['is_check'=>$bool_data['data']['is_check']]);
        if($boola || $boolb){
            $message = new MessageController();
            if ($bool_data['data']['is_check'] == 1){
                $memberBasicModel = new MemberBasic();
                $basic = $memberBasicModel->getBasicKeys(['uid'=>$data['uid']],['name','uid']);
                $message->addMessage($data['uid'],'4',$basic->name.'复审通过，待生成卡片');//后端消息
                $message->addMessage($data['uid'],'3','您的人才龙卡申请已复审通过');//前端消息
            }elseif ($bool_data['data']['is_check'] == 4){
                $message->addMessage($data['uid'],'3','您的人才龙卡复审驳回');
            }elseif ($bool_data['data']['is_check'] == 5){
                $message->addMessage($data['uid'],'3','您的人才龙卡复审不通过');
            }
            return response()->json(['code'=>'1','message'=>'审核成功']);
        }else{
            return response()->json(['code'=>'0','message'=>'审核失败']);
        }
    }


    /**
     * 验证
     * @param $data
     * @return array
     */
    public function getValidator($data)
    {
        $validator = \Validator::make($data,[
            'uid'=>'required|integer',
            'is_check'=>'required|integer|max:10',
            //'remark'=>'required',
        ],[
            'required'=>':attribute为必填项',
            'max'=>':attribute长度不符合要求',
            'integer'=>':attribute必须为数字',
        ],[
            'mobile'=>'用户id',
            'is_check'=>'审核结果',
            //'remark'=>'备注',
        ]);


        //验证失败，并返回第一个报错
        if($validator->fails()) {
            return ['code' => 0, 'message' => $validator->errors()->first()];
        }


        if($data['is_check'] == 1){
            if(empty($data['remark'])){
                $data['remark'] = '';
            }
        }else{
            if(empty($data['remark'])){
                return ['code'=>'0','message'=>'原因不能为空'];
            }

            //判断是否是审核不通过
            if($data['is_check'] == 2){
                if(empty($data['freeze_time'])){
                    return ['code'=>'0','message'=>'过期天数必须大于0'];
                }elseif (!is_int($data['freeze_time'])){
                    return ['code'=>'0','message'=>'过期天数必须大于0的整数'];
                }else{
                    if($data['freeze_time'] <= 0){
                        return ['code'=>'0','message'=>'过期天数必须大于0'];
                    }
                    $temp['is_freeze'] = 1;
                    $temp['freeze_time'] = strtotime("+".$data['freeze_time']." days",strtotime(date('Y-m-d',time())));
                }
            }
        }

        $temp['updtime'] = time();

        //获取龙卡状态
        $dragon = $this->memberDragonModel->getDragon(['uid'=>$data['uid']],['uid','is_check']);
        if(empty($dragon)){
            return ['code'=>'0','message'=>'用户信息不存在'];
        }

        if(!in_array($dragon->is_check,['2','3'])){
            return ['code'=>'0','message'=>'此用户您不能审核'];
        }elseif ($dragon->is_check == 2){
            $temp['first_remark'] = $data['remark'];
            $temp['first_trial_user'] = $data['user']['uid'];
            $temp['first_trial_time'] = $temp['updtime'];
        }else{
            $temp['second_remark'] = $data['remark'];
            $temp['second_trial_user'] = $data['user']['uid'];
            $temp['second_trial_time'] = $temp['updtime'];
        }


        if($data['is_check'] == 1 && $dragon->is_check == 3){
            $validator = \Validator::make($data,[
                'card_num'=>'required',
                'onset_time'=>'required',
                'over_time'=>'required',
                //'remark'=>'required',
            ],[
                'required'=>':attribute为必填项',
                'max'=>':attribute长度不符合要求',
                'integer'=>':attribute必须为数字',
            ],[
                'card_num'=>'龙卡卡号',
                'onset_time'=>'有效期',
                'over_time'=>'有效期',
                //'remark'=>'备注',
            ]);


            //验证失败，并返回第一个报错
            if($validator->fails()) {
                return ['code' => 0, 'message' => $validator->errors()->first()];
            }

            $card_num = $this->memberDragonModel->getDragon(['card_num'=>$data['card_num']],['uid']);
            if(!empty($card_num)){
                return ['code' => '0', 'message' => '此卡号已存在'];
            }

            $temp['card_num'] = $data['card_num'];
            $temp['onset_time'] = strtotime($data['onset_time']);
            $temp['over_time'] = strtotime($data['over_time']);
            $temp['is_check'] = 1;
        }elseif ($data['is_check'] == 1 && $dragon->is_check == 2){
            $temp['is_check'] = 3;
        }elseif ($data['is_check'] == 2) {
            if (empty($data['remark'])) {
                return ['code' => '0', 'message' => '不通过原因不能为空'];
            }
            $temp['is_check'] = 5;
        }elseif ($data['is_check'] == 3){
            if (empty($data['remark'])) {
                return ['code' => '0', 'message' => '驳回原因不能为空'];
            }
            $temp['is_check'] = 4;
        }

        return ['code'=>1,'message'=>'验证成功','data'=>$temp];
    }

    public function getDetails(Request $request)
    {
        if(!$request->has('uid')){
            return response()->json(['code'=>'0','message'=>'请上传用户uid']);
        }

        $member = $this->memberModel->getDetails($request->input('uid'));
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$member]);
    }

    public function getUserZige(Request $request)
    {
        if(!$request->has('uid')){
            return response()->json(['code'=>'0','message'=>'请上传用户uid']);
        }

        $memberInfoModel= new MemberBasicInfo();

        $dragon = $memberInfoModel->getBasicInfo(['uid'=>$request->input('uid')]);
        if(empty($dragon)){
            return response()->json(['code'=>'0','message'=>'该用户还为申请龙卡']);
        }
        if(empty($dragon->img)){
            $img = [];
        }else{
            $img = json_decode($dragon->img,true);
        }
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$img]);
    }


    public function getBasics()
    {
        $dragon = new DragonController();
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$dragon->basiceType()]);
    }

    /**
     * 添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyDragonCard(Request $request)
    {
        $data = $request->input();
        $data['is_update'] = 1;
        $dragon = new DragonController();
        $bool_data = $dragon->getValidator($data,1);
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }

        $member = $this->memberModel->getMember(['mobile'=>$bool_data['data']['mobile']],['id','is_check']);
        if(!empty($member)){
            if($member->is_check != 0){
                return response()->json(['code'=>'0','message'=>'此手机号已经申请了']);
            }
        }else{
            $temp['mobile'] = $bool_data['data']['mobile'];
            $temp['name'] = $bool_data['data']['name'];
            $temp['user_id'] = $bool_data['data']['user']['uid'];
            $member = $this->memberModel->create($temp);
            if(empty($member)){
                return response()->json(['code'=>'0','message'=>'申请失败']);
            }
        }

        return $this->chuli($bool_data,$member,1);
    }

    /**
     * 修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updApplyDragonCard(Request $request)
    {
        $data = $request->input();
        if(empty($data['is_update'])){
            return response()->json(['code'=>'0','message'=>'请上传接口类型']);
        }

        $dragon = new DragonController();
        $bool_data = $dragon->getValidator($data,1);
        if($bool_data['code'] == 0){
            $result['code'] = '0';
            $result['message'] = $bool_data['message'];
            return response()->json($result);
        }

        if(empty($data['uid'])){
            return response()->json(['code'=>'0','message'=>'请上传用户id']);
        }

        $member = $this->memberModel->getMember(['id'=>$data['uid']],['id','is_check','user_id']);
        if(empty($member)){
            return response()->json(['code'=>'0','message'=>'用户不存在']);
        }elseif ($member->user_id == 0){
            return response()->json(['code'=>'0','message'=>'用户自己添加的您不能修改']);
        }

        if($data['is_update'] == 2){
            $dragonModel = new MemberDragon();
            $dragon_info = $dragonModel->getDragon(['uid' => $data['uid']], ['is_check','is_freeze','freeze_time']);

            if($dragon_info->is_check != 5){
                return response()->json(['code'=>'0','message'=>'接口请求错误']);
            }elseif ($dragon_info->freeze_time > time()){
                return response()->json(['code'=>'0','message'=>'冻结时间未到，您不能修改']);
            }
        }

        unset($bool_data['data']['uid']);
        return $this->chuli($bool_data,$member,2);
    }


    public function removeApplyDragonCard(Request $request)
    {
        if(!$request->has('uid')){
            return response()->json(['code'=>'0','message'=>'请上传用户id']);
        }
        $uid = $request->input('uid');
        $member = $this->memberModel->where('id',$uid)->where('user_id','>','0')->where('is_check','2')->first(['id']);
        if(empty($member)){
            return response()->json(['code'=>'0','message'=>'该用户不能删除']);
        }

        #开启事务
        DB::beginTransaction();
        $memdber_bool = $this->memberModel->where('id',$member->id)->delete();
        $dragon_bool = $this->memberDragonModel->where('uid',$member->id)->delete();
        $qiyeModel = new MemberEnterprise();
        $qiye_bool = $qiyeModel->where('uid',$member->id)->delete();
        $xinxiModel = new MemberBasic();
        $xinxi_bool = $xinxiModel->where('uid',$member->id)->delete();
        $xqModel = new MemberBasicInfo();
        $xq_bool = $xqModel->where('uid',$member->id)->delete();
        if($memdber_bool && $dragon_bool && $qiye_bool && $xinxi_bool && $xq_bool) {
            DB::commit();
            return response()->json(['code'=>'1','message'=>'删除成功']);
        }else{
            DB::rollBack();
            return response()->json(['code'=>'0','message'=>'删除失败']);
        }

    }


    public function chuli($bool_data,$info,$type)
    {

        $bool_data['data']['user']['uid'] = $info->id;
        $qiyeModel = new MemberEnterprise();
        $qiyeModel->addEnterprise($bool_data['data']);

        $basicModel = new MemberBasic();
        $basicModel->addBasic($bool_data['data']);

        $basicInfoModel = new MemberBasicInfo();
        $basicInfoModel->addBasicInfo($bool_data['data']);

        $dragonModel = new MemberDragon();
        $dragonModel->addDragon($bool_data['data']);

        $message = new MessageController();
        $message->addMessage($info->id,'1','人才龙卡申请待审核',['is_check'=>'2']);


        $member_dragon_info = $dragonModel->getDragon(['uid' => $info->id], ['is_check']);
        if ($type == 1) {
            if(!$this->memberModel->updMember(['id'=>$info->id],['is_check'=>2])){
                return response()->json(['code'=>'0','message'=>'申请失败']);
            }
        }elseif(in_array($member_dragon_info->is_check,['4'])){
            $result = $dragonModel->updDragon(['id'=>$info->id],['is_check'=>2]);
            if ($result['code'] == 0) {
                return response()->json($result);
            }

            if(!$this->memberModel->updMember(['id'=>$info->id],['is_check'=>2])){
                return response()->json(['code'=>'0','message'=>'修改失败']);
            }
        }

        if($bool_data['data']['is_update'] == 2){
            if(in_array($member_dragon_info->is_check,['5'])){
                $result = $dragonModel->updDragon(['id'=>$info->id],['is_check'=>2]);
                if ($result['code'] == 0) {
                    return response()->json($result);
                }

                if(!$this->memberModel->updMember(['id'=>$info->id],['is_check'=>2])){
                    return response()->json(['code'=>'0','message'=>'修改失败']);
                }
            }
        }


        if($type == 1){
            return response()->json(['code'=>'1','message'=>'申请成功']);
        }else{
            return response()->json(['code'=>'1','message'=>'修改成功']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkImport(Request $request)
    {
        if(!$request->has('excel')){
            $result = [
                'code' => '0',
                'message' => '请上传excel表格',
            ];
            return response()->json($result);
        }

        $file = $request->file('excel');
        if(!$file->isValid()){
            $result = [
                'code' => '0',
                'message' => '上传失败',
            ];
            return response()->json($result);
        }

        //扩展名
        $ext = $file->getClientOriginalExtension();
        if(!in_array($ext,['xls','xlsx'])){
            $result = [
                'code' => '0',
                'message' => '格式错误',
            ];
            return response()->json($result);
        }

        $user = $request->input();
        try {
            Excel::import(new MemberDragonImports($user),$request->file('excel'));
        }catch (\Exception $e){
            return response()->json(['code'=>0,'message'=>$e->getMessage()]);
        }
        $result = [
            'code' => '1',
            'message' => '操作成功',
        ];
        return response()->json($result);
    }


    /**
     * 导出
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getMemberExcel()
    {
        $temp['0'] = [
            '单位名称',
            '单位性质',
            '所属行业',
            '企业类型',
            '注册资金',
            '年产值',
            '单位地址',
            '单位区域',
            '联系人',
            '联系电话',
            '真实姓名',
            '联系电话',
            '身份证号码',
            '出生地',
//            '居住地址',
            '国籍',
            '户籍所在地',
            '学历',
            '学位',
//            '人才类别',
            '毕业院校',
            '毕业时间',
            '职称',
            '职务',
            '人才符合条件',
            '专业领域',
            '引才方式',
//            '来区时间',
            '与本单位首次签订劳动合同/协议的时间',
            '社保购买地',
            '引进方式',
//            '柔性引进在区工作',
            '主要学习与工作经历',
            '主要业绩情况',
            '近期免冠照片',
            '申报“人才龙卡”服务项目（多个项目用逗号隔开）',
            '相关资料',
//            '居住区域',
            '荣誉内容',
            '荣誉等级',
            '专利',
            '著作',
            '社会影响力',
            '社会或市场认可情况、创新创业能力等情况',
            '状态',
        ];


        $members = $this->memberModel
            ->join('member_basic', 'member.id', '=', 'member_basic.uid')
            ->join('member_basicinfo', 'member.id', '=', 'member_basicinfo.uid')
            ->join('member_enterprise', 'member.id', '=', 'member_enterprise.uid')
            ->where('member.is_check','>','0')
            ->select([
                'member_enterprise.enterprise_name',  //企业名称
                'member_enterprise.area_range',  //企业区域
                'member_enterprise.nature_unit', //单位性质
                'member_enterprise.enterprise_type', //企业类型
                'member_enterprise.firm_name',  //行业名称
                'member_enterprise.registered_capital',  //注册资金
                'member_enterprise.annual_value',  //年产值
                'member_enterprise.enterprise_address',  //企业地址
                'member_enterprise.lianxiren',  //联系人
                'member_enterprise.enterprise_mobile',  //电话
                'member_basic.name',  //姓名
                'member_basic.mobile',  //电话
                'member_basic.identity_num',  //身份证
                'member_basic.birthplace',  //出身地
//                'member_basic.jz_address',  //居住地址
//                'member_basic.jz_area_range',  //居住区域
                'member_basic.nationality', //国籍
                'member_basic.hk_address',  //户口所在地
                'member_basic.xueli',  //学历
                'member_basic.xuewei',  //学位
                'member_basic.talents_type',  //人才类型
                'member_basic.graduate_school',  //毕业院校
                'member_basic.graduate_time',  //毕业时间
                'member_basic.honor_content',  //荣誉等级
                'member_basic.honor_grade',  //荣誉内容
                'member_basicinfo.technical_level',  //职称
                'member_basicinfo.duty',  //职务
                'member_basicinfo.talent_match',  //人才符合条件
                'member_basicinfo.professional_field',  //专业领域
                'member_basicinfo.introduce_way',  //引进方式
                'member_basicinfo.come_district_time',  //来区时间
                'member_basicinfo.service_contract_time',  //合同签订时间
                'member_basicinfo.social_security_address',  //社保购买地方
                'member_basicinfo.introduce_way_describe',  //引进方式
//                'member_basicinfo.job_day',  //工作时间
                'member_basicinfo.personal_experience',  //个人经历
                'member_basicinfo.performance',  //个人业绩
                'member_basicinfo.avatar',  //头像
                'member_basicinfo.service_project',  //服务项目
                'member_basicinfo.img',  //相关资料
                'member_basicinfo.patent',  //专利
                'member_basicinfo.book',  //著作
                'member_basicinfo.social_influence',  //社会影响力
                'member_basicinfo.recognition',  //社会或市场认可情况、创新创业能力等情况
                'member.is_check',  //状态
            ])->get();


        $dragon = new DragonController();
        $basice_type = $dragon->basiceType();
        foreach ($members as $key=>$value)
        {
            $key = $key + 1;
            $temp[$key]['enterprise_name'] = $value->enterprise_name;
            $temp[$key]['nature_unit'] = $basice_type['nature_unit'][$value->nature_unit];
            $temp[$key]['firm_name'] = $value->firm_name;
            $temp[$key]['enterprise_type'] = $value->firm_name;
            $temp[$key]['registered_capital'] = $value->registered_capital;
            $temp[$key]['annual_value'] = $value->annual_value;
            $temp[$key]['enterprise_address'] = $value->enterprise_address;
            $temp[$key]['area_range'] = $value->area_range;
            $temp[$key]['lianxiren'] = ' '.$value->lianxiren;
            $temp[$key]['enterprise_mobile'] = $value->enterprise_mobile;
            $temp[$key]['name'] = $value->name;
            $temp[$key]['mobile'] = ' '.$value->mobile;
            $temp[$key]['identity_num'] = ' '.(string)$value->identity_num;
            $temp[$key]['birthplace'] = $value->birthplace;
//            $temp[$key]['jz_address'] = $value->jz_address;
            $temp[$key]['nationality'] = $value->nationality;
            $temp[$key]['hk_address'] = $value->hk_address;
            $temp[$key]['xueli'] = $basice_type['xueli'][$value->xueli];
            $temp[$key]['xuewei'] = $basice_type['xuewei'][$value->xuewei];
//            $temp[$key]['talents_type'] = $basice_type['talents_type'][$value->talents_type];
            $temp[$key]['graduate_school'] = $value->graduate_school;
            $temp[$key]['graduate_time'] = $value->graduate_time;
            $temp[$key]['technical_level'] = $value->technical_level;
            $temp[$key]['duty'] = $value->duty;
            $temp[$key]['talent_match'] = $value->talent_match;
            $temp[$key]['professional_field'] = $basice_type['professional_field'][$value->professional_field];
            $temp[$key]['introduce_way'] = $basice_type['introduce_way'][$value->introduce_way];
//            $temp[$key]['come_district_time'] = date('Y-m-d',$value->come_district_time);
            $temp[$key]['service_contract_time'] = $value->service_contract_time;
            $temp[$key]['social_security_address'] = $value->social_security_address;
            $temp[$key]['introduce_way_describe'] = $value->introduce_way_describe;
//            $temp[$key]['job_day'] = $value->job_day;
            $temp[$key]['personal_experience'] = $value->personal_experience;
            $temp[$key]['performance'] = $value->performance;
            $temp[$key]['avatar'] = $value->avatar;
            $temp[$key]['service_project'] = ServiceProject::id2title($value->service_project);
            $temp[$key]['img'] = $value->img;
//            $temp[$key]['jz_area_range'] = $value->jz_area_range;
            $temp[$key]['honor_content'] = $value->honor_content;
            $temp[$key]['honor_grade'] = $value->honor_grade;
            $temp[$key]['patent'] = $value->patent;
            $temp[$key]['book'] = $value->book;
            $temp[$key]['social_influence'] = $value->social_influence;
            $temp[$key]['recognition'] = $value->recognition;
            if($value->is_check == 1){
                $is_check = '审核通过';
            }elseif ($value->is_check  == 2){
                $is_check = '待初审';
            }elseif ($value->is_check  == 3){
                $is_check = '待复审';
            }elseif ($value->is_check  == 4){
                $is_check = '批回';
            }elseif ($value->is_check  == 5){
                $is_check = '不通过';
            }

            $temp[$key]['is_check'] = $is_check;

        }

        $export = new MemberExports($temp);
        $name = '龙卡信息'.date('Y-m-d H:i:s');
        $download =  Excel::download($export,$name.'.xls');
        $file = $download->getFile();

        $upload = new ImageController();
        $path = $upload->getTypePath(3);

        if($path['code'] == 0){
            $result = [
                'code' => '0',
                'message' => '文件上传路径有误',
            ];
            return response()->json($result);
        }

        $pathName = $path['path'].$name.'.xls';
        $aly = $upload->AliyunOss($pathName,file_get_contents($file->getRealPath()));
        if($aly['info']['http_code'] != 200){
            return response()->json([
                'message'=>'上传失败',
                'code'=>'0',
            ]);
        }

        $result['code'] = '1';
        $result['message'] = '获取成功';
        $result['path'] = $aly['oss-request-url'];
        return response()->json($result);
    }
}
