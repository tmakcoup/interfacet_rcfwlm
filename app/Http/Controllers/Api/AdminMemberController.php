<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Member;
use Illuminate\Http\Request;

class AdminMemberController extends Controller
{
    private $memberModel;

    public function __construct()
    {
        $this->memberModel = new Member();
    }

    public function list(Request $request)
    {
        if(!$request->has('page_size'))
            return response()->json(['code'=>'0','message'=>'请上传页面条数']);

        $data = $request->input();
        $list = $this->memberModel->getMemberList($data);
        return response()->json(['code'=>'1','message'=>'获取成功','data'=>$list]);
    }

    
}
