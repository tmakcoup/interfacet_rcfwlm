<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weixin extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'weixin';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function addWeixin($data)
    {
        return $this->insert($data);
    }

    public function getWeixin($where)
    {
        return $this->where($where)->first();
    }
}
