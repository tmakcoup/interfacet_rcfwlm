<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-29
 * Time: 10:48
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberEnterprise extends Model
{

    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'member_enterprise';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;


    public function addEnterprise($data)
    {
        $temp = $this->settleData($data);
        $qiye = $this->getEnterprise(['uid'=>$data['user']['uid']]);
        if(!empty($qiye)){
            return $this->updEnterprise($temp['uid'],$temp);
        }

        if($this->insert($temp)){
            return ['code'=>'1','message'=>'添加成功'];
        }else{
            return ['code'=>'0','message'=>'添加失败'];
        }
    }

    public function updEnterprise($uid,$data)
    {
        unset($data['uid']);
        if($this->where('uid',$uid)->update($data)){
            return ['code'=>'1','message'=>'修改成功'];
        }else{
            return ['code'=>'0','message'=>'修改失败'];
        }
    }

    public function settleData($data)
    {
        $temp['enterprise_name'] = $data['enterprise_name'];
        $temp['area_range'] = $data['area_range'];
        $temp['nature_unit'] = $data['nature_unit'];
        $temp['firm_name'] = $data['firm_name'];
        $temp['registered_capital'] = $data['registered_capital'];
        $temp['annual_value'] = $data['annual_value'];
        $temp['enterprise_address'] = $data['enterprise_address'];
        $temp['lianxiren'] = $data['lianxiren'];
        $temp['enterprise_mobile'] = $data['enterprise_mobile'];
        $temp['enterprise_type'] = $data['enterprise_type'];
        $temp['uid'] = $data['user']['uid'];
        $temp['updtime'] = time();
        return $temp;
    }

    public function getEnterprise($where)
    {
        return $this->where($where)->first();
    }
}
