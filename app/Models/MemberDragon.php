<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-05-06
 * Time: 10:47
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberDragon extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'member_dragon';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function addDragon($data)
    {
        $temp = $this->settleData($data);
        $qiye = $this->getDragon(['uid'=>$data['user']['uid']],['uid']);
        if(!empty($qiye)){
            if($data['user']['guard'] == 'admin') {
                unset($temp['is_check']);
            }else{
                $temp['first_trial_user'] = 0;
                $temp['first_trial_time'] = null;
                $temp['second_trial_user'] = 0;
                $temp['second_trial_time'] = null;
                $temp['first_remark'] = null;
                $temp['second_remark'] = null;
            }
            return $this->updDragon($temp['uid'],$temp);
        }

        if($this->insert($temp)){
            return ['code'=>'1','message'=>'添加成功'];
        }else{
            return ['code'=>'0','message'=>'添加失败'];
        }
    }

    public function updDragon($uid,$data)
    {
        unset($data['uid']);
        if($this->where('uid',$uid)->update($data)){
            return ['code'=>'1','message'=>'修改成功'];
        }else{
            return ['code'=>'0','message'=>'修改失败'];
        }
    }

    public function settleData($data)
    {
        $temp['uid'] = $data['user']['uid'];
        $temp['is_check'] = 2;
        $temp['is_freeze'] = 0;
        $temp['updtime'] = time();
        return $temp;
    }

    /**
     * @param $where
     * @param $key
     * @return mixed
     */
    public function getDragon($where,$key)
    {
        return $this->where($where)->first($key);
    }
}
