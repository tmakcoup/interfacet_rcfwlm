<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-29
 * Time: 16:10
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberBasicInfo extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'member_basicinfo';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;


    public function addBasicInfo($data)
    {
        $temp = $this->settleData($data);
        $qiye = $this->getBasicInfo(['uid'=>$data['user']['uid']]);
        if(!empty($qiye)){
            return $this->updBasicInfo($temp['uid'],$temp);
        }

        if($this->insert($temp)){
            return ['code'=>'1','message'=>'添加成功'];
        }else{
            return ['code'=>'0','message'=>'添加失败'];
        }
    }

    public function updBasicInfo($uid,$data)
    {
        unset($data['uid']);
        if($this->where('uid',$uid)->update($data)){
            return ['code'=>'1','message'=>'修改成功'];
        }else{
            return ['code'=>'0','message'=>'修改失败'];
        }
    }

    public function settleData($data)
    {
        $temp['technical_level'] = $data['technical_level'];
        $temp['duty'] = $data['duty'];
        $temp['professional_field'] = $data['professional_field'];
        $temp['introduce_way'] = $data['introduce_way'];
        if($data['introduce_way'] == 1){
            $temp['service_contract_time'] = $data['service_contract_time'];
            $temp['social_security_address'] = $data['social_security_address'];
        }else{
            $temp['introduce_way_describe'] = $data['introduce_way_describe'];
            $temp['job_day'] ='';
        }
//        $temp['come_district_time'] = strtotime($data['come_district_time']);
        $temp['personal_experience'] = $data['personal_experience'];
        $temp['performance'] = $data['performance'];
        $temp['avatar'] = $data['avatar'];
        $temp['service_project'] = $data['service_project'];
        $temp['uid'] = $data['user']['uid'];
        $temp['updtime'] = time();
        $temp['talent_match'] = $data['talent_match'];
        $temp['patent'] = $data['patent'];
        $temp['book'] = $data['book'];
        $temp['social_influence'] = $data['social_influence'];
        $temp['recognition'] = $data['recognition'];
        if(!empty($data['img'])){
            $temp['img'] = json_encode($data['img']);
        }
        return $temp;
    }

    public function getBasicInfo($where)
    {
        return $this->where($where)->first();
    }
}
