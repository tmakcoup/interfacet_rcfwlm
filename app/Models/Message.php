<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'message';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function addMessage($data)
    {
        return $this->insert($data);
    }
}
