<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-29
 * Time: 15:25
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberBasic extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'member_basic';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;


    public function addBasic($data)
    {
        $temp = $this->settleData($data);
        $qiye = $this->getBasic(['uid'=>$data['user']['uid']]);
        if(!empty($qiye)){
            unset($temp['addtime']);
            return $this->updBasic($temp['uid'],$temp);
        }

        if($this->insert($temp)){
            return ['code'=>'1','message'=>'添加成功'];
        }else{
            return ['code'=>'0','message'=>'添加失败'];
        }
    }

    public function updBasic($uid,$data)
    {
        unset($data['uid']);
        if($this->where('uid',$uid)->update($data)){
            return ['code'=>'1','message'=>'修改成功'];
        }else{
            return ['code'=>'0','message'=>'修改失败'];
        }
    }

    public function settleData($data)
    {
        $temp['name'] = $data['name'];
        $temp['mobile'] = $data['mobile'];
        $temp['identity_num'] = $data['identity_num'];
        $temp['birthplace'] = $data['birthplace'];
        $temp['nationality'] = $data['nationality'];
//        $temp['jz_address'] = $data['jz_address'];
//        $temp['jz_area_range'] = $data['jz_area_range'];
        $temp['hk_address'] = $data['hk_address'];
        $temp['xueli'] = $data['xueli'];
        $temp['xuewei'] = $data['xuewei'];
        $temp['graduate_school'] = $data['graduate_school'];
        $temp['graduate_time'] = $data['graduate_time'];
        $temp['birthday'] = $data['birthday'];
        $temp['sex'] = $data['sex'];
        $temp['uid'] = $data['user']['uid'];
        $temp['addtime'] = time();
        $temp['updtime'] = $temp['addtime'];
        $temp['talents_type'] = 1;
        $temp['honor_content'] = $data['honor_content'];
        $temp['honor_grade'] = $data['honor_grade'];
        return $temp;
    }

    public function getBasic($where)
    {
        return $this->where($where)->first();
    }

    public function getBasicKeys($where,$key)
    {
        return $this->where($where)->first($key);
    }
}
