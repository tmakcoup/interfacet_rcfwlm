<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuggestFeedback extends Model
{

    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'suggest_feedback';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function addSuggestFeedback($data)
    {
        return $this->insert($data);
    }

    public function getSuggestFeedbackList($where)
    {
        $info = $this
            ->join('member', 'member.id', '=', 'suggest_feedback.uid')


            ->when(!empty($where['title']),function ($query) use ($where){
                $query->where('suggest_feedback.title','like','%'.$where['mobile'].'%');
            })

            //开始时间
            ->when(!empty($where['onset_time']),function ($query) use ($where){
                $onset_time = strtotime($where['onset_time']);
                $query->where('suggest_feedback.addtime','>',$onset_time);
            })

            //结束时间
            ->when(!empty($where['over_time']),function ($query) use ($where){
                $over_time = strtotime($where['over_time']);
                $query->where('suggest_feedback.addtime','<',$over_time);
            })
            ->orderBy('suggest_feedback.addtime','desc')
            ->select([
                'suggest_feedback.id',
                'member.mobile',
                'suggest_feedback.content',
                'suggest_feedback.addtime'
            ])
            ->paginate($where['page_size']);

        foreach ($info as $key=>$value)
        {
            $info[$key]->addtime = date('Y-m-d H:i:s',$value->addtime);
        }

        return $info;
    }
}
