<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationService extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'reservation_service';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function addReservation($data)
    {
        if($id = $this->insertGetId($data)){
            return ['code'=>'1','message'=>'预约成功','id'=>$id];
        }else{
            return ['code'=>'0','message'=>'预约失败'];
        }
    }

    public function updReservation($where,$key)
    {
        return $this->where($where)->update($key);
    }


    public function getReservation($where,$key)
    {
        return $this->where($where)->first($key);
    }

    public function getDetails($where)
    {
        $info = $this->where($where)->first();
        if(empty($info)){
            return ['code'=>'0','message'=>'请选择服务'];
        }

        $service = new ServiceProject();
        $info->item_arr = $service->getProject(['id'=>$info->item_id],['title','describe']);
        $info->addtime = date('Y-m-d H:i:s',$info->addtime);
        $info->onset_time = date('Y-m-d H:i',$info->onset_time);
        $info->over_time = date('Y-m-d H:i',$info->over_time);

        if(!empty($info->sl_time)){
            $info->sl_time = date('Y-m-d H:i:s',$info->sl_time);
        }else{
            $info->sl_time = '';
        }

        if(!empty($info->wc_time)){
            $info->wc_time = date('Y-m-d H:i:s',$info->wc_time);
        }else{
            $info->wc_time = '';
        }

        if(!empty($info->qx_time)){
            $info->qx_time = date('Y-m-d H:i:s',$info->qx_time);
        }else{
            $info->qx_time = '';
        }

        unset($info->updtime);

        return ['code'=>'1','message'=>'获取成功','data'=>$info];
    }

    public function disposeCancel($where)
    {
        $info = $this->getReservation($where,['id','status']);
        if(empty($info)){
            return ['code'=>'0','message'=>'请选择服务'];
        }

        if($info->status != 1){
            return ['code'=>'0','message'=>'此服务您不能取消'];
        }

        $temp['status'] = 4;
        $temp['qx_time'] = time();
        $temp['updtime'] = $temp['qx_time'];

        $bool = $this->updReservation($where,$temp);
        if($bool){
            return ['code'=>'1','message'=>'取消成功'];
        }else{
            return ['code'=>'0','message'=>'取消失败'];
        }
    }

    /**
     * 前端
     * @param $where
     * @return mixed
     */
    public function getApiReservationServiceList($where)
    {
        $yuyue = $this->where('uid',$where['uid'])->select(['id','item_id','onset_time','over_time','status'])->orderBy('addtime','desc')->paginate($where['page_size']);
        $service = new ServiceProject();
        foreach ($yuyue as $key=>$value){
            $yuyue[$key]->item_arr = $service->getProject(['id'=>$value->item_id],['title','describe']);
            $yuyue[$key]->onset_time = date('Y/m/d H:i',$value->onset_time);
            $yuyue[$key]->over_time = date('Y/m/d H:i',$value->over_time);
        }
        return $yuyue;
    }

    public function getCount($where)
    {
        return $this->where('uid',$where['uid'])->where('status','<','4')->select(['id'])->count();
    }

    public function getUserServiceList($where)
    {
        $yuyue = $this
            ->where('uid',$where['uid'])
            ->whereIn('status',['2','3'])
            ->select(['id','item_id','onset_time','over_time','status','updtime','sl_user'])
            ->get();
        $service = new ServiceProject();
        $adminModel = new Admin();
        foreach ($yuyue as $key=>$value){
            $yuyue[$key]->item_arr = $service->getProject(['id'=>$value->item_id],['title','describe']);
            $yuyue[$key]->onset_time = date('Y/m/d',$value->onset_time);
            $yuyue[$key]->over_time = date('Y/m/d',$value->over_time);
            $yuyue[$key]->updtime = date('Y/m/d H:i:s',$value->updtime);

            $admin = $adminModel->getAdmin(['id'=>$value->sl_user],['name']);
            $yuyue[$key]->sl_user = $admin->name;
        }
        return $yuyue;
    }

    /**
     * 后台列表
     * @param $where
     * @return mixed
     */
    public function getReservationServiceList($where)
    {
        $members = $this
            ->join('member_basic', 'reservation_service.uid', '=', 'member_basic.uid')

            //是否审核
            ->when(!empty($where['is_status']),function ($query) use ($where){
                if($where['is_status'] != 10){
                    $query->where('reservation_service.status',$where['is_status']);
                }
            })

            ->when(!empty($where['item_id']),function ($query) use ($where){
                $query->where('reservation_service.item_id',$where['item_id']);
            })

            //用户姓名
            ->when(!empty($where['name']),function ($query) use ($where){
                $query->where('member_basic.name','like','%'.$where['name'].'%');
            })

            //用户电话
            ->when(!empty($where['mobile']),function ($query) use ($where){
                $query->where('member_basic.mobile','like','%'.$where['mobile'].'%');
            })

            //开始时间
            ->when(!empty($where['onset_time']),function ($query) use ($where){
                $onset_time = strtotime($where['onset_time']);
                if(in_array($where['is_status'],['1'])){
                    $query->where('reservation_service.addtime','>',$onset_time);
                }else{
                    $query->where('reservation_service.sl_time','>',$onset_time);
                }
            })

            //结束时间
            ->when(!empty($where['over_time']),function ($query) use ($where){
                $over_time = strtotime($where['over_time']);
                if(in_array($where['is_status'],['1'])){
                    $query->where('reservation_service.addtime','<',$over_time);
                }else{
                    $query->where('reservation_service.sl_time','<',$over_time);
                }
            })
            ->orderBy('addtime','desc')
            ->select([
                'reservation_service.*',
                'member_basic.name',
                'member_basic.mobile',
                'member_basic.identity_num',
            ])
            ->paginate($where['page_size']);


        $service = new ServiceProject();
        $adminModel = new Admin();
        foreach ($members as $key=>$value){
            $members[$key]->item_arr = $service->getProject(['id'=>$value->item_id],['title','describe']);
            $members[$key]->addtime = date('Y-m-d H:i:s',$value->addtime);
            $members[$key]->onset_time = date('Y-m-d H:i:s',$value->onset_time);
            $members[$key]->over_time = date('Y-m-d H:i:s',$value->over_time);

            if(!empty($value->sl_time)){
                $members[$key]->sl_time = date('Y-m-d H:i:s',$value->sl_time);
            }else{
                $members[$key]->sl_time = '';
            }

            if(!empty($value->wc_time)){
                $members[$key]->wc_time = date('Y-m-d H:i:s',$value->wc_time);
            }else{
                $members[$key]->wc_time = '';
            }
            if(empty($value->sl_user)){
                $members[$key]->sl_user = '';
            }else{
                $admin = $adminModel->getAdmin(['id'=>$value->sl_user],['name']);
                $members[$key]->sl_user = $admin->name;
            }


            unset($members[$key]->qx_time);
            unset($members[$key]->updtime);
        }

        return $members;
    }
}
