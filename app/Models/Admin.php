<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-23
 * Time: 15:31
 */
namespace App\Models;

use App\Http\Controllers\Api\DragonController;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mobile', 'password','description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * @param $data
     * @return mixed
     */
    public function addAdmin($data)
    {
        $admin = $this->getAdmin(['mobile'=>$data['mobile'],'status'=>'1'],['id','mobile']);
        if(!empty($admin)){
            $resule['code'] = '0';
            $resule['message'] = '此账号已添加';
            return $resule;
        }

        //密码加密
        $data['password'] = bcrypt($data['password']);
        $role_id = $data['role_id'];
        unset($data['role_id']);
        if($info = $this->create($data)){
            $this->setRole($info->id,$role_id);
            $resule['code'] = '1';
            $resule['message'] = '添加成功';
        }else{
            $resule['code'] = '0';
            $resule['message'] = '添加失败，请重新添加';
        }

        return $resule;
    }

    public function updAdmin($where,$data)
    {
        //密码加密
        if($data['password'] == '-----'){
            unset($data['password']);
        }else{
            $data['password'] = bcrypt($data['password']);
        }
        $role_id = $data['role_id'];
        unset($data['role_id']);
        if($this->where($where)->update($data)){
            $this->setRole($where['id'],$role_id);
            $resule['code'] = '1';
            $resule['message'] = '修改成功';
        }else{
            $resule['code'] = '0';
            $resule['message'] = '修改失败，请重新操作';
        }

        return $resule;
    }


    public function setRole($id,$role_id)
    {
        $info  = $this->getAdmin(['id'=>$id],['id']);
        $roleUser = new RoleUser();
        $roleUser->getDelete(['user_id'=>$id]);
        return $info->roles()->attach($role_id);
    }


    /**
     * @param $where
     * @param $key
     * @return mixed
     */
    public function getAdmin($where,$key)
    {
        return $this->where($where)->first($key);
    }

    public function getAdminsList($where)
    {
        $admins = $this

            ->where('status',1)
            //用户姓名
            ->when(!empty($where['name']),function ($query) use ($where){
                $query->where('name','like','%'.$where['name'].'%');
            })
            ->when(!empty($where['mobile']),function ($query) use ($where){
                $query->where('mobile','like','%'.$where['mobile'].'%');
            })
            //开始时间
            ->when(!empty($where['onset_time']),function ($query) use ($where){
                $onset_time = strtotime($where['onset_time']);
                $query->where('created_at','>',$onset_time);
            })

            //结束时间
            ->when(!empty($where['over_time']),function ($query) use ($where){
                $over_time = strtotime($where['over_time']);
                $query->where('created_at','<',$over_time);
            })
            ->select([
                'id',
                'mobile',
                'name',
                'created_at',
            ])
            ->orderBy('id','desc')
            ->paginate($where['page_size']);

        foreach ($admins as $key=>$value){
            $admins[$key]->role_user = $this->getRouleName($value->id);
        }

        return $admins;

    }

    /**
     * @param $user_id
     * @return string
     */
    public function getRouleName($user_id)
    {
        $roleUserModel = new RoleUser();
        $role_user = $roleUserModel->getRoleUser(['user_id'=>$user_id]);
        if(empty($role_user)){
            return '';
        }

        $roleModel = new Role();

        $role = $roleModel->getRoles(['id'=>$role_user['role_id']],['id','display_name']);
        return $role->display_name;
    }

}
