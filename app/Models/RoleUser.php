<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-24
 * Time: 14:07
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class RoleUser extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'role_user';

    public function getDelete($where)
    {
        return $this->where($where)->delete();
    }

    public function getRoleUser($where)
    {
        return $this->where($where)->first();
    }

    public function getRoleUserAll($where)
    {
        return $this->where($where)->get();
    }


    public function getRoleUsersByPermissionId($permission_id) {
        return DB::select("SELECT u.user_id FROM rcjh_permissions p, rcjh_permission_role r, rcjh_role_user u WHERE p.id = r.permission_id AND r.role_id = u.role_id AND r.permission_id = $permission_id");

    }
}
