<?php
namespace App\Models;

use App\Http\Controllers\Api\DragonController;
use Illuminate\Database\Eloquent\Model;

class MemberTalents extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'member_talents';

    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function addTalents($data)
    {
        $temp = $this->settleData($data);

        $qiye = $this->getTalents(['uid'=>$data['user']['uid']]);
        $temp['updtime'] = time();
        if(!empty($qiye)){
            if($data['user']['guard'] == 'admin'){
                if($data['is_update'] == 2){
                    if(!in_array($qiye->is_check,['4'])){
                        unset($temp['is_check']);
                    }
                }else{
                    if(!in_array($qiye->is_check,['3'])){
                        unset($temp['is_check']);
                    }
                }
            }

            unset($temp['user_id']);
            return $this->updTalents($temp['uid'],$temp);
        }
        $temp['addtime'] = $temp['updtime'];
        if($this->insert($temp)){
            return ['code'=>'1','message'=>'添加成功'];
        }else{
            return ['code'=>'0','message'=>'添加失败'];
        }
    }


    public function updTalents($uid,$data)
    {
        unset($data['uid']);
        if($this->where('uid',$uid)->update($data)){
            return ['code'=>'1','message'=>'修改成功'];
        }else{
            return ['code'=>'0','message'=>'修改失败'];
        }
    }

    public function settleData($data)
    {
        $temp['uid'] = $data['user']['uid'];
        $temp['enterprise_name'] = $data['enterprise_name'];
        $temp['area_range'] = $data['area_range'];
        $temp['nature_unit'] = $data['nature_unit'];
        $temp['firm_name'] = $data['firm_name'];
        $temp['talents_type'] = $data['talents_type'];
        $temp['honor_content'] = $data['honor_content'];
        $temp['honor_grade'] = $data['honor_grade'];
        $temp['name'] = $data['name'];
        $temp['mobile'] = $data['mobile'];
        $temp['identity_num'] = $data['identity_num'];
        $temp['mingzu'] = $data['mingzu'];
        $temp['birthplace'] = $data['birthplace'];
        $temp['social_security_address'] = $data['social_security_address'];
        $temp['come_district_time'] = $data['come_district_time'];
        $temp['xueli'] = $data['xueli'];
        $temp['xuewei'] = $data['xuewei'];
        $temp['technical_level'] = $data['technical_level'];
        $temp['duty'] = $data['duty'];
        $temp['jz_address'] = $data['jz_address'];
        $temp['jz_area_range'] = $data['jz_area_range'];
        $temp['personal_experience'] = $data['personal_experience'];
        $temp['avatar'] = $data['avatar'];
        $temp['birthday'] = $data['birthday'];
        $temp['sex'] = $data['sex'];
        $temp['is_check'] = 2;
        //$temp['is_freeze'] = 0;
        $temp['user_id'] = $data['user_id'];
        if(!empty($data['img'])){
            $temp['img'] = json_encode($data['img']);
        }
        return $temp;
    }

    public function getTalents($where)
    {
        return $this->where($where)->first();
    }

    public function getMemberListTalents($where)
    {
        $members = $this
            //是否审核
            ->when(!empty($where['is_check']),function ($query) use ($where){
                if($where['is_check'] != 10){
                    $query->where('is_check',$where['is_check']);
                }
            })

            //人才类型
            ->when(!empty($where['talents_type']),function ($query) use ($where){
                if($where['talents_type'] != 10){
                    $query->where('talents_type',$where['talents_type']);
                }
            })

            //用户姓名
            ->when(!empty($where['name']),function ($query) use ($where){
                $query->where('name','like','%'.$where['name'].'%');
            })

            //用户电话
            ->when(!empty($where['mobile']),function ($query) use ($where){
                $query->where('mobile','like','%'.$where['mobile'].'%');
            })

            //开始时间
            ->when(!empty($where['onset_time']),function ($query) use ($where){
                $onset_time = strtotime($where['onset_time']);
                $query->where('addtime','>',$onset_time);
            })

            //结束时间
            ->when(!empty($where['over_time']),function ($query) use ($where){
                $over_time = strtotime($where['over_time']);
                $query->where('addtime','<',$over_time);
            })
            ->orderBy('addtime','desc')
            ->select([
                'uid',
                'name',
                'mobile',
                'identity_num',
                'talents_type',
                'is_check',
                'trial_user',
                'trial_time',
                'remark',
                'user_id',
                'is_freeze',
                'addtime',
            ])
            ->paginate($where['page_size']);

        $dragon = new DragonController();
        $basice = $dragon->basiceType();

        foreach ($members as $key=>$value){
            $members[$key]->talents_type = $basice['talents_type'][$value->talents_type];
            if(in_array($where['is_check'],['10','2'])){
                $members[$key]->time = date('Y-m-d H:i:s',$value->addtime);
                if(empty($value->trial_time)){
                    $members[$key]->trial_time = '';
                    $members[$key]->trial_use = '';
                }else{
                    $members[$key]->trial_time = date('Y-m-d H:i:s',$value->trial_time);
                    $members[$key]->trial_use = $this->getAdminName($value->trial_user);
                }
            }else{
                $members[$key]->time = date('Y-m-d H:i:s',$value->addtime);
                $members[$key]->trial_time = date('Y-m-d H:i:s',$value->trial_time);
                $members[$key]->trial_use = $this->getAdminName($value->trial_user);
            }

            unset($value['addtime']);
            //unset($value['trial_time']);
        }

        return $members;
    }


    public function getAdminName($id)
    {
        $adminModel = new Admin();
        $admin = $adminModel->where('id',$id)->first(['name']);
        return $admin->name;
    }

    public function getDetails($uid)
    {
        $info = $this->where('uid',$uid)->first();
        if(empty($info->img)){
            $info->img = [];
        }else{
            $info->img = json_decode($info->img,true);
        }

        unset($info->addtime);
        unset($info->updtime);
        return $info;
    }
}
