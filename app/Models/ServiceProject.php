<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-28
 * Time: 10:33
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProject extends Model
{
    /**
     * 与模型关联的数据表
     *
     * @var string
     */
    protected $table = 'service_project';


    /**
     * 该模型是否被自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;


    public function addProject($data)
    {
        $project = $this->getProject(['title'=>$data['title'],'status'=>'1'],['id']);
        if(!empty($project)){
            $resule['code'] = '0';
            $resule['message'] = '此项目已添加';
            return $resule;
        }

        $temp['title'] = $data['title'];
        $temp['describe'] = $data['describe'];
        $temp['time'] = time();

        if($this->insert($temp)){
            $resule['code'] = '1';
            $resule['message'] = '添加成功';
        }else{
            $resule['code'] = '0';
            $resule['message'] = '添加失败，请重新添加';
        }

        return $resule;
    }


    public function updProject($data)
    {
        $info = $this->getProject(['id'=>$data['id'],'status'=>'1'],['id']);
        if(empty($info)){
            $resule['code'] = '0';
            $resule['message'] = '乖乖数据异常';
            return $resule;
        }

        $temp['title'] = $data['title'];
        $temp['describe'] = $data['describe'];
        $temp['time'] = time();

        if($this->where('id',$data['id'])->update($temp)){
            $resule['code'] = '1';
            $resule['message'] = '修改成功';
        }else{
            $resule['code'] = '0';
            $resule['message'] = '修改失败';
        }

        return $resule;
    }

    public function getDelect($id)
    {
        $info = $this->getProject(['id'=>$id,'status'=>'1'],['id']);
        if(empty($info)){
            $resule['code'] = '0';
            $resule['message'] = '数据异常';
            return $resule;
        }

        $temp['status'] = 0;
        $temp['time'] = time();

        if($this->where('id',$id)->update($temp)){
            $resule['code'] = '1';
            $resule['message'] = '删除成功';
        }else{
            $resule['code'] = '0';
            $resule['message'] = '删除失败';
        }

        return $resule;
    }

    public function getProject($where,$key)
    {
        return $this->where($where)->first($key);
    }

    public function getServiceProjectList($where)
    {
        $list = $this
            ->when(!empty($where['title']),function ($query) use ($where){
                $query->where('title','like','%'.$where['title'].'%');
            })

            //开始时间
            ->when(!empty($where['onset_time']),function ($query) use ($where){
                $onset_time = strtotime($where['onset_time']);
                $query->where('time','>',$onset_time);
            })

            //结束时间
            ->when(!empty($where['over_time']),function ($query) use ($where){
                $over_time = strtotime($where['over_time']);
                $query->where('time','<',$over_time);
            })
            ->where('status','1')
            ->orderBy('id','desc')
            ->select([
                'id',
                'title',
                'describe',
                'time',
                'status',
                'time',
            ])
            ->paginate($where['page_size']);

        foreach ($list as $key=>$value){
            $list[$key]->time = date('Y-m-d H:i:s',$value->time);
        }

        return $list;
    }

    /**
     * @param $str
     * @return string
     * @throws \Exception
     */
    public static function title2id($str){
        $projects = explode('、',$str);
        $allProject = self::all();
        $temp = [];
        foreach($projects as $v){
            $project = $allProject->where('title',$v)->first();
            if(!$project)
                throw new \Exception('服务项目标题错误：'.$v);
            $temp[] = $project->id;
        }
        return implode(',',$temp);
    }

    /**
     * @param $nums
     * @return string
     * @throws \Exception
     */
    public static function id2title($nums){
        $allProject = self::all();
        $temp = [];
        $projects = explode(',',$nums);
        foreach($projects as $v){
            $project  = $allProject->where('id',$v)->first();
            if(!$project)
                throw new \Exception('服务项目存储错误id：'.$v);
            $temp[] = $project->title;
        }
        return implode('、',$temp);
    }
}
