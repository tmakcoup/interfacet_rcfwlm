<?php

namespace App\Imports;

use App\Http\Controllers\Api\DragonController;
use App\Http\Controllers\Api\TalentsInfoController;
use App\Models\Member;
use App\Models\MemberTalents;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class MemberTalentsImports implements ToCollection
{
    private $user;
    private $memberModel;
    private $memberTalentsModel;

    public function __construct($user)
    {
        $this->user = $user;
        $this->memberModel = new Member();
        $this->memberTalentsModel = new MemberTalents();
    }

    public function collection(Collection $collection)
    {
        // TODO: Implement collection() method.
        $collections = $collection->toArray();
        $dragon = new DragonController();
        $basice_type = $this->setBasiceType($dragon);

        $talents = new TalentsInfoController();
        foreach ($collections as $key=>$value)
        {
            if($key > 1){
                $bool_data = $this->fieldClearUp($value,$basice_type,$talents);
                if($bool_data['code'] == 0){
                    throw new \Exception($bool_data['message']);
                }

                $bool = $this->addTalents($bool_data);

                if($bool['code'] == 0){
                    throw new \Exception($bool['message']);
                }
            }
        }

        return ['code'=>'1','message'=>'添加成功'];
    }

    public function addTalents($bool_data)
    {

        $member = $this->memberModel->getMember(['mobile'=>$bool_data['data']['mobile']],['id','mobile']);
        if(empty($member)){
            $temp['mobile'] = $bool_data['data']['mobile'];
            $temp['name'] = $bool_data['data']['name'];
            $temp['user_id'] = $this->user['user']['uid'];
            $member = $this->memberModel->create($temp);
            if(empty($member)){
                return response()->json(['code'=>'0','message'=>'添加失败']);
            }
        }
        $bool_data['data']['user']['uid'] = $member->id;

        $talents = $this->memberTalentsModel->getTalents(['uid'=>$member->id]);
        if(!empty($talents->uid)){
            return ['code'=>'0','message'=>'已经添加'];
        }

        $bool_data['data']['user_id'] = $this->user['user']['uid'];
        return $this->memberTalentsModel->addTalents($bool_data['data']);
    }

    public function fieldClearUp($data,$basice_type,$talents)
    {
        foreach($data as &$v){
            $v = trim($v);
        }
        $temp['enterprise_name'] = $data['0'];
        if(empty($basice_type['nature_unit'][$data['1']])){
            return ['code'=>'0','message'=>'单位性质请选择：党政机关、事业单位、企业或其他经济社会组织'];
        }
        $temp['nature_unit'] = $basice_type['nature_unit'][$data['1']];
        $temp['firm_name'] = $data['2'];

        if(empty($basice_type['talents_type'][$data['3']])){
            return ['code'=>'0','message'=>'人才类别请选择：专业技术人才、技能人才、管理人才'];
        }
        $temp['talents_type'] = $basice_type['talents_type'][$data['3']];
        $temp['name'] = $data['4'];
        $temp['mobile'] = $data['5'];
        $temp['identity_num'] = $data['6'];
        $temp['mingzu'] = $data['7'];
        $temp['birthplace'] = $data['8'];
        $temp['social_security_address'] = $data['9'];
        $temp['come_district_time'] = $data['10'];
        if(empty($basice_type['xueli'][$data['11']])){
            return ['code'=>'0','message'=>'学历请选择：博士、硕士、本科、大专及以下'];
        }
        $temp['xueli'] = $basice_type['xueli'][$data['11']];

        if(!key_exists($data['12'],$basice_type['xuewei'])){
            return ['code'=>'0','message'=>'学位请选择：博士研究生、硕士研究生、学士、无'];
        }
        $temp['xuewei'] = $basice_type['xuewei'][$data['12']];

        $temp['technical_level'] = $data['13'];
        $temp['duty'] = $data['14'];
        $temp['jz_address'] = $data['15'];
        $temp['personal_experience'] = $data['16'];
        $temp['area_range'] = $data['19'];
        $temp['jz_area_range'] = $data['20'];
        $temp['honor_content'] = $data['21'];
        $temp['honor_grade'] = $data['22'];
        $temp['avatar'] = 'https://xianruo-data.oss-cn-hangzhou.aliyuncs.com/fishery-shop/uploads/images/shop/2018-10-17/5bc6ad802efa4.png';
        $temp['img'] = [];

        return $talents->getValidator($temp,1);
    }

    public function setBasiceType($dragon)
    {
        $jclx = $dragon->basiceType();
        foreach ($jclx as $key=>$value){
            $jclx[$key] = array_flip($value);
        }

        return $jclx;
    }
}
