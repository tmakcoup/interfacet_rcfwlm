<?php
namespace App\Imports;


use App\Http\Controllers\Api\DragonController;
use App\Http\Controllers\Api\MessageController;
use App\Models\Member;
use App\Models\MemberBasic;
use App\Models\MemberBasicInfo;
use App\Models\MemberDragon;
use App\Models\MemberEnterprise;
use App\Models\ServiceProject;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class MemberDragonImports implements ToCollection
{

    private $user;
    private $memberModel;
    private $qiyeModel;
    private $basicModel;
    private $basicInfoModel;
    private $dragonModel;

    public function __construct($user)
    {
        $this->user = $user;
        $this->memberModel = new Member();
        $this->qiyeModel = new MemberEnterprise();
        $this->basicModel = new MemberBasic();
        $this->basicInfoModel = new MemberBasicInfo();
        $this->dragonModel = new MemberDragon();
    }


    public function collection(Collection $collection)
    {
        // TODO: Implement collection() method.
        $collections = $collection->toArray();


        $dragon = new DragonController();
        $basice_type = $this->setBasiceType($dragon);
        foreach ($collections as $key=>$value)
        {
            if($key > 1){
                $bool_data = $this->newFieldClearUp($value,$basice_type,$dragon);
                if($bool_data['code'] == 0){
                    throw new \Exception($bool_data['message']);
                }


                $bool = $this->excelApplyDragonCard($bool_data['data']);
                if($bool['code'] == 0){
                    throw new \Exception($bool['message']);
                }
            }
        }

        return ['code'=>'1','message'=>'添加成功'];
    }


    /**
     * 龙卡申请
     * @param $data
     * @return array
     */
    public function excelApplyDragonCard($data)
    {

        $member = $this->memberModel->getMember(['mobile'=>$data['mobile']],['id','is_check']);
        if(!empty($member)){
            return ['code'=>'0','message'=>"手机号{$data['mobile']}已经注册了"];
        }

        $temp['mobile'] = $data['mobile'];
        $temp['name'] = $data['name'];
        $temp['user_id'] = $this->user['user']['uid'];

        $info = $this->memberModel->create($temp);
        if(empty($info)){
            return ['code'=>'0','message'=>'申请失败'];
        }

        return $this->chuli(['data'=>$data],$info);
    }

    /**
     * @param $bool_data
     * @param $info
     * @return array
     */
    public function chuli($bool_data,$info)
    {

        $bool_data['data']['user']['uid'] = $info->id;
        $this->qiyeModel->addEnterprise($bool_data['data']);

        $this->basicModel->addBasic($bool_data['data']);
        $this->basicInfoModel->addBasicInfo($bool_data['data']);

        $this->dragonModel->addDragon($bool_data['data']);

        if($this->memberModel->updMember(['id'=>$info->id],['is_check'=>2])){
            $message = new MessageController();
            $message->addMessage($info->id,'1','人才龙卡申请待审核',['is_check'=>'2']);
            return ['code'=>'1','message'=>'申请成功'];
        }else{
            return ['code'=>'0','message'=>'申请失败'];
        }
    }

    /**
     * 字段处理及验证
     * @param $data
     * @param $basice_type
     * @param $dragon
     * @return array
     */
    public function fieldClearUp($data,$basice_type,$dragon)
    {
        foreach($data as &$v){
            $v = trim($v);
        }
        $temp['enterprise_name'] = $data['0'];
        if(empty($basice_type['nature_unit'][$data['1']])){
            return ['code'=>'0','message'=>'单位性质请选择：党政机关、事业单位、企业或其他经济社会组织'];
        }
        $temp['nature_unit'] = $basice_type['nature_unit'][$data['1']];
        $temp['firm_name'] = $data['2'];
        $temp['registered_capital'] = $data['3'];
        $temp['annual_value'] = $data['4'];
        $temp['enterprise_address'] = $data['5'];
        $temp['lianxiren'] = $data['6'];
        $temp['enterprise_mobile'] = $data['7'];
        $temp['name'] = $data['8'];
        $temp['mobile'] = $data['9'];
        $temp['identity_num'] = $data['10'];
        $temp['birthplace'] = $data['11'];
        $temp['jz_address'] = $data['12'];
        $temp['nationality'] = $data['13'];
        $temp['hk_address'] = $data['14'];

        if(empty($basice_type['xueli'][$data['15']])){
            return ['code'=>'0','message'=>'学历请选择：博士、硕士、本科、大专及以下'];
        }
        $temp['xueli'] = $basice_type['xueli'][$data['15']];

        if(!key_exists($data['16'],$basice_type['xuewei'])){
            return ['code'=>'0','message'=>'学位请选择：博士研究生、硕士研究生、学士、无'];
        }
        $temp['xuewei'] = $basice_type['xuewei'][$data['16']];

        if(empty($basice_type['talents_type'][$data['17']])){
            return ['code'=>'0','message'=>'人才类别请选择：专业技术人才、技能人才、管理人才'];
        }
        $temp['talents_type'] = $basice_type['talents_type'][$data['17']];
        $temp['graduate_school'] = $data['18'];
        $temp['graduate_time'] = $data['19'];
        $temp['technical_level'] = $data['20'];
        $temp['duty'] = $data['21'];

        if(empty($basice_type['introduce_way'][$data['22']])){
            return ['code'=>'0','message'=>'引入方式请选择:刚性引进、柔性引进'];
        }
        $temp['introduce_way'] = $basice_type['introduce_way'][$data['22']];

        if(empty($basice_type['professional_field'][$data['23']])){
            return ['code'=>'0','message'=>'专业领域选择错误'];
        }
        $temp['professional_field'] = $basice_type['professional_field'][$data['23']];
        $temp['come_district_time'] = $data['24'];
        $temp['service_contract_time'] = $data['25'];
        $temp['social_security_address'] = $data['26'];
        $temp['introduce_way_describe'] = $data['27'];
        $temp['job_day'] = $data['28'];
        $temp['personal_experience'] = $data['29'];
        $temp['performance'] = $data['30'];
        $temp['area_range'] = $data['34'];
        $temp['jz_area_range'] = $data['35'];
        $temp['honor_content'] = $data['36'];
        $temp['honor_grade'] = $data['37'];
        $temp['talent_match'] = $data['38'];
        $temp['patent'] = $data['39'];
        $temp['book'] = $data['40'];
        $temp['social_influence'] = $data['41'];
        $temp['recognition'] = $data['42'];
        $temp['enterprise_type'] = $data['43'];
        $temp['avatar'] = 'https://xianruo-data.oss-cn-hangzhou.aliyuncs.com/fishery-shop/uploads/images/shop/2018-10-17/5bc6ad802efa4.png';
        $temp['service_project'] = implode(',',$basice_type['service_project']);
        $temp['img'] = [$temp['avatar']];
        return $dragon->getValidator($temp,1);
    }

    /**
     * 字段处理及验证
     * @param $data
     * @param $basice_type
     * @param $dragon
     * @return array
     */

    public function newFieldClearUp($data,$basice_type,$dragon)
    {
        $fields = [
            'enterprise_name','nature_unit','firm_name','enterprise_type','registered_capital','annual_value','enterprise_address',
            'area_range','lianxiren','enterprise_mobile','name','mobile','identity_num','birthplace','nationality','hk_address',
            'xueli','xuewei','graduate_school','graduate_time','technical_level','duty','talent_match','introduce_way',
            'professional_field','service_contract_time','social_security_address','introduce_way_describe','personal_experience',
            'performance','honor_content','honor_grade','patent','book','social_influence','recognition','service_project'
        ];
        $temp = [];
        foreach($fields as $k=>$v){
            $temp[$v] = trim($data[$k]);
        }
        if(empty($basice_type['nature_unit'][$temp['nature_unit']])){
            return ['code'=>'0','message'=>'单位性质请选择：党政机关、事业单位、企业或其他经济社会组织'];
        }
        $temp['nature_unit'] = $basice_type['nature_unit'][$temp['nature_unit']];
        if(empty($basice_type['xueli'][$temp['xueli']])){
            return ['code'=>'0','message'=>'学历请选择：博士、硕士、本科、大专及以下'];
        }
        $temp['xueli'] = $basice_type['xueli'][$temp['xueli']];
        if(!key_exists($temp['xuewei'],$basice_type['xuewei'])){
            return ['code'=>'0','message'=>'学位请选择：博士研究生、硕士研究生、学士、无'];
        }
        if(!key_exists($temp['enterprise_type'],$basice_type['enterprise_type'])){
            return ['code'=>'0','message'=>'企业类型请选择：国有企业、民营企业、有限合伙、中外合资、其他'];
        }
        $temp['xuewei'] = $basice_type['xuewei'][$temp['xuewei']];
//        if(empty($basice_type['talents_type'][$temp['talents_type']])){
//            return ['code'=>'0','message'=>'人才类别请选择：专业技术人才、技能人才、管理人才'];
//        }
//        $temp['talents_type'] = $basice_type['talents_type'][$temp['talents_type']];
        if(empty($basice_type['introduce_way'][$temp['introduce_way']])){
            return ['code'=>'0','message'=>'引入方式请选择:刚性引进、柔性引进'];
        }
        $temp['introduce_way'] = $basice_type['introduce_way'][$temp['introduce_way']];
        if(empty($basice_type['professional_field'][$temp['professional_field']])){
            return ['code'=>'0','message'=>'专业领域选择错误'];
        }
        $temp['professional_field'] = $basice_type['professional_field'][$temp['professional_field']];
        $temp['avatar'] = 'https://xianruo-data.oss-cn-hangzhou.aliyuncs.com/fishery-shop/uploads/images/shop/2018-10-17/5bc6ad802efa4.png';
        $temp['service_project'] = ServiceProject::title2id($temp['service_project']);
        $temp['img'] = [$temp['avatar']];
        return $dragon->getValidator($temp,1);
    }

    /**
     * @return mixed
     */
    public function setBasiceType($dragon)
    {
        $jclx = $dragon->basiceType();
        foreach ($jclx as $key=>$value){
            $jclx[$key] = array_flip($value);
        }

        return $jclx;
    }
}
