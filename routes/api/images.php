<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-25
 * Time: 11:37
 */

//图片管理模块
$api->group(['prefix' => 'image'], function ($api) {
    $api->post('uploadImg',['as'=>'api.image.uploadImg','uses' => 'ImageController@uploadImg']);
});