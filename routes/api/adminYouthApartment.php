<?php
// 房源信息管理
$api->group(['prefix' => 'listingInformation'],  function ($api){
	// 小区管理
    $api->post('editCommunity', ['as'=>'api.listingInformation.editCommunity','uses' => 'ListingInformationController@editCommunity']);
    $api->post('deleteCommunity', ['as'=>'api.listingInformation.deleteCommunity','uses' => 'ListingInformationController@deleteCommunity']);
    $api->get('communitys', ['as'=>'api.listingInformation.communitys','uses' => 'ListingInformationController@communityList']);
    $api->get('community/{community_id}', ['as'=>'api.listingInformation.communityInfo','uses' => 'ListingInformationController@communityInfo']);

    // 楼栋管理
    $api->post('editBuilding', ['as'=>'api.listingInformation.editBuilding','uses' => 'ListingInformationController@editBuilding']);
    $api->post('deleteBuilding', ['as'=>'api.listingInformation.deleteBuilding','uses' => 'ListingInformationController@deleteBuilding']);
    $api->get('buildings', ['as'=>'api.listingInformation.buildings','uses' => 'ListingInformationController@buildingList']);
    $api->get('building/{building_id}', ['as'=>'api.listingInformation.buildingInfo','uses' => 'ListingInformationController@buildingInfo']);

    // 户型管理
    $api->post('editHouseType', ['as'=>'api.listingInformation.editHouseType','uses' => 'ListingInformationController@editHouseType']);
    $api->post('deleteHouseType', ['as'=>'api.listingInformation.deleteHouseType','uses' => 'ListingInformationController@deleteHouseType']);
    $api->get('houseTypes', ['as'=>'api.listingInformation.houseTypes','uses' => 'ListingInformationController@houseTypeList']);
    $api->get('houseType/{house_type_id}', ['as'=>'api.listingInformation.houseTypeInfo','uses' => 'ListingInformationController@houseTypeInfo']);

    // 房号管理
    $api->post('editRoomNo', ['as'=>'api.listingInformation.editRoomNo','uses' => 'ListingInformationController@editRoomNo']);
    $api->post('deleteRoomNo', ['as'=>'api.listingInformation.deleteRoomNo','uses' => 'ListingInformationController@deleteRoomNo']);
    $api->get('roomNos', ['as'=>'api.listingInformation.roomNos','uses' => 'ListingInformationController@roomNoList']);
    $api->get('roomNo/{room_no_id}', ['as'=>'api.listingInformation.roomNoInfo','uses' => 'ListingInformationController@roomNoInfo']);
    $api->get('units', ['as'=>'api.listingInformation.units','uses' => 'ListingInformationController@unitList']);
    $api->get('floors', ['as'=>'api.listingInformation.floors','uses' => 'ListingInformationController@floorList']);
    $api->post('roomNoImport', ['as'=>'api.listingInformation.roomNoImport','uses' => 'ListingInformationController@roomNoImport']);
});

// 公寓申请管理
$api->group(['prefix' => 'apartmentApplication'],  function ($api){
    $api->post('editApartmentApplication', ['as'=>'api.apartmentApplication.editApartmentApplication','uses' => 'ApartmentApplicationController@editApartmentApplication']);
    $api->get('apartmentApplications', ['as'=>'api.apartmentApplication.apartmentApplications','uses' => 'ApartmentApplicationController@apartmentApplicationList']);
    $api->get('apartmentApplication/{apartment_application_id}', ['as'=>'api.apartmentApplication.apartmentApplicationInfo','uses' => 'ApartmentApplicationController@apartmentApplicationInfo']);
    $api->post('approval', ['as'=>'api.apartmentApplication.approval','uses' => 'ApartmentApplicationController@apartmentApplicationApproval']);
    $api->post('deleteApplication', ['as'=>'api.apartmentApplication.deleteApplication','uses' => 'ApartmentApplicationController@deleteApartmentApplication']);
    $api->get('approvals', ['as'=>'api.apartmentApplication.approvals','uses' => 'ApartmentApplicationController@applicationApprovalList']);
    $api->post('distributionHouse', ['as'=>'api.apartmentApplication.distributionHouse','uses' => 'DistributionController@distributionHouse']);
    $api->get('distributionHouses', ['as'=>'api.apartmentApplication.distributionHouses','uses' => 'DistributionController@distributionHouses']);
    $api->post('apartmentApplicationImport', ['as'=>'api.apartmentApplication.apartmentApplicationImport','uses' => 'ApartmentApplicationController@apartmentApplicationImport']);
    $api->get('apartmentApplicationExport', ['as'=>'api.apartmentApplication.apartmentApplications','uses' => 'ApartmentApplicationController@ApartmentApplicationExports']);
});

// 公寓分配管理
$api->group(['prefix' => 'distribution'],  function ($api){
    $api->get('roomNos', ['as'=>'api.distribution.roomNos','uses' => 'DistributionController@distributionRoomNoList']);
    $api->post('checkin', ['as'=>'api.distribution.checkin','uses' => 'DistributionController@distributionCheckin']);
    $api->post('free', ['as'=>'api.distribution.free','uses' => 'DistributionController@distributionFree']);
    $api->get('giveUps', ['as'=>'api.distribution.giveUps','uses' => 'DistributionController@giveUpApplicationList']);
    $api->get('changeRooms', ['as'=>'api.distribution.changeRooms','uses' => 'DistributionController@changeRoomApplicationList']);
    $api->post('changeRoomApproval', ['as'=>'api.distribution.changeRoomApproval','uses' => 'DistributionController@changeRoomApproval']);
});

// 公寓续租管理
$api->group(['prefix' => 'renewal'],  function ($api){
    $api->get('renewalAppicationList', ['as'=>'api.renewal.renewalAppicationList','uses' => 'RenewalController@renewalAppicationList']);
    $api->post('renewalApproval', ['as'=>'api.renewal.renewalApproval','uses' => 'RenewalController@renewalApproval']);
    $api->get('withdrawalAppicationList', ['as'=>'api.renewal.withdrawalAppicationList','uses' => 'RenewalController@withdrawalAppicationList']);
    $api->post('withdrawalApproval', ['as'=>'api.renewal.withdrawalApproval','uses' => 'RenewalController@withdrawalApproval']);
    $api->get('situationReflectionList', ['as'=>'api.renewal.situationReflectionList','uses' => 'RenewalController@situationReflectionList']);
    $api->post('situationReflectionHandle', ['as'=>'api.renewal.situationReflectionHandle','uses' => 'RenewalController@situationReflectionHandle']);
    $api->get('houseRecordsList', ['as'=>'api.renewal.houseRecordsList','uses' => 'RenewalController@houseRecordsList']);
});