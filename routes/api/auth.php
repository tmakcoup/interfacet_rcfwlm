<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-23
 * Time: 15:14
 */

//管理员
$api->group(['prefix' => 'admins'],  function ($api){
     $api->get('list', ['as'=>'api.admins.list','uses' => 'AdminsController@list']);
     $api->get('details', ['as'=>'api.admins.details','uses' => 'AdminsController@details']);
     $api->post('addAdmins', ['as'=>'api.admins.addAdmins','uses' => 'AdminsController@addAdmins']);
     $api->post('updAdmins', ['as'=>'api.admins.updAdmins','uses' => 'AdminsController@updAdmins']);
     $api->get('removeAdmins', ['as'=>'api.admins.removeAdmins','uses' => 'AdminsController@removeAdmins']);
     $api->post('changePassword', ['as'=>'api.admins.changePassword','uses' => 'AdminsController@changePassword']);
});


 //角色
 $api->group(['prefix' => 'roles'],  function ($api){
     $api->get('index', ['as'=>'api.roles.index','uses' => 'RolesController@index']);
     $api->post('addRoles', ['as'=>'api.roles.addRoles','uses' => 'RolesController@addRoles']);
     $api->get('allocation', ['as'=>'api.roles.allocation','uses' => 'RolesController@allocation']);
     $api->get('removeRoles', ['as'=>'api.roles.removeRoles','uses' => 'RolesController@removeRoles']);
     $api->post('updRoles', ['as'=>'api.roles.updRoles','uses' => 'RolesController@updRoles']);
 });



//权限
$api->group(['prefix' => 'permissions'],  function ($api){
    $api->get('index', ['as'=>'api.permissions.index','uses' => 'PermissionsController@index']);
    $api->post('addPermission', ['as'=>'api.permissions.addPermission','uses' => 'PermissionsController@addPermission']);
    $api->post('updPermission', ['as'=>'api.permissions.updPermission','uses' => 'PermissionsController@updPermission']);
});
