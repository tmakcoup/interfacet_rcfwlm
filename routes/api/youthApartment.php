<?php
$api->group(['prefix' => 'apartment'],  function ($api){
    $api->post('apartmentApplication', ['as'=>'api.apartment.apartmentApplication','uses' => 'ApartmentController@apartmentApplication']);
    $api->post('renewalAppication', ['as'=>'api.apartment.renewalAppication','uses' => 'ApartmentController@renewalAppication']);
    $api->post('withdrawalAppication', ['as'=>'api.apartment.withdrawalAppication','uses' => 'ApartmentController@withdrawalAppication']);
    $api->post('situationReflection', ['as'=>'api.apartment.situationReflection','uses' => 'ApartmentController@situationReflection']);
    $api->get('schedule', ['as'=>'api.apartment.schedule','uses' => 'ApartmentController@schedule']);
    $api->get('renewalInfo', ['as'=>'api.apartment.renewalInfo','uses' => 'ApartmentController@renewalInfo']);
    $api->get('withdrawalInfo', ['as'=>'api.apartment.withdrawalInfo','uses' => 'ApartmentController@withdrawalInfo']);
    $api->get('situationReflectionInfo', ['as'=>'api.apartment.situationReflectionInfo','uses' => 'ApartmentController@situationReflectionInfo']);
    $api->get('apartmentApplicationInfo', ['as'=>'api.apartment.apartmentApplicationInfo','uses' => 'ApartmentController@apartmentApplicationInfo']);
    $api->get('apartmentApplicationStatus', ['as'=>'api.apartment.apartmentApplicationStatus','uses' => 'ApartmentController@apartmentApplicationStatus']);
    $api->post('giveUpApplication', ['as'=>'api.apartment.giveUpApplication','uses' => 'ApartmentController@giveUpApplication']);
    $api->post('changeRoomApplication', ['as'=>'api.apartment.changeRoomApplication','uses' => 'ApartmentController@changeRoomApplication']);
    $api->get('changeRoomInfo', ['as'=>'api.apartment.changeRoomInfo','uses' => 'ApartmentController@changeRoomInfo']);
});