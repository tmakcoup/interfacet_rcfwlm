<?php
$api->group(['prefix' => 'adminPcImg'],  function ($api){
    $api->post('subImg', ['as'=>'api.adminPcImg.subImg','uses' => 'AdminPcImgController@subImg']);
    $api->get('getImg', ['as'=>'api.adminPcImg.getImg','uses' => 'AdminPcImgController@getImg']);
});
