<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-05-07
 * Time: 09:59
 */
$api->group(['prefix' => 'adminManageCard'],  function ($api){
    $api->get('createCard', ['as'=>'api.manageCard.createCard','uses' => 'AdminManageCardController@createCard']);
    $api->get('drawCard', ['as'=>'api.manageCard.drawCard','uses' => 'AdminManageCardController@drawCard']);
});
