<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-23
 * Time: 15:05
 */

$api = app('Dingo\Api\Routing\Router');


$api->version('v1',['namespace' => 'App\Http\Controllers\Api'],function ($api) {
    //登录
    include "login.php";

    //微信
    include "weachat.php";

    //图片管理
    include 'images.php';

    //验证码获取
    $api->group(['prefix' => 'noteCode'], function ($api) {
        $api->get('getCode',['as'=>'api.noteCode.getCode','uses' => 'NoteCodeController@getCode']);
    });

    //用户信息
    $api->group(['middleware' => ['refresh.token:member']], function ($api) {
        $api->group(['prefix' => 'member'], function ($api) {
            $api->post('updMobile', ['as' => 'api.member.updMobile', 'uses' => 'MemberController@updMobile']);
        });
    });
    //政策发布
    $api->group(['prefix' => 'releasePolicy'], function ($api) {
        $api->get('weixinList',['as'=>'api.releasePolicy.weixinList','uses' => 'ReleasePolicyController@weixinList']);
        $api->get('weixinDetails',['as'=>'api.releasePolicy.weixinDetails','uses' => 'ReleasePolicyController@weixinDetails']);
    });

    //定时任务
    $api->group(['prefix' => 'timing'], function ($api) {
        $api->get('message',['as'=>'api.timing.message','uses' => 'TimingController@message']);
    });


    //pc首页图片
    $api->group(['prefix' => 'pcImg'], function ($api) {
        $api->get('getImg', ['as'=>'api.pcImg.getImg','uses' => 'AdminPcImgController@getImg']);
    });

    //前端
    $api->group(['middleware' => ['refresh.token:member']], function ($api) {

        //表单token
        $api->group(['prefix' => 'formToken'], function ($api) {
            $api->get('getToken',['as'=>'api.formToken.getToken','uses' => 'FormTokenController@getToken']);
        });

        //消息
        $api->group(['prefix' => 'apiMessage'], function ($api) {
            $api->get('apiList',['as'=>'api.apiMessage.apiList','uses' => 'MessageController@apiList']);
            $api->post('apiMessageRead',['as'=>'api.apiMessage.apiMessageRead','uses' => 'MessageController@apiMessageRead']);
        });

        //龙卡申请
        include 'dragon.php';

        // 青年公寓
        include 'youthApartment.php';

        //反馈建议
        include 'suggest.php';
    });

    //后台
    $api->group(['middleware' => ['refresh.token:admin','checkauth:admin']], function ($api) {

        $api->group(['prefix' => 'index'], function ($api) {
            $api->get('index',['as'=>'api.index.index','uses' => 'IndexController@index']);
        });

        //消息
        $api->group(['prefix' => 'adminMessage'], function ($api) {
            $api->get('adminList',['as'=>'api.adminMessage.adminList','uses' => 'MessageController@adminList']);
            $api->post('adminMessageRead',['as'=>'api.adminMessage.adminMessageRead','uses' => 'MessageController@adminMessageRead']);
        });

        //权限
        include "auth.php";

        //服务项目
        include "service.php";

        //龙卡审核
        include 'adminDragon.php';

        // 青年公寓
        include 'adminYouthApartment.php';
        
        //卡片管理
        include 'adminManageCard.php';

        //政策发布
        include 'releasePolicy.php';

        //会员列表
        include 'adminMember.php';

        //反馈建议
        include "adminSuggest.php";

        //pc图片
        include 'adminPcImg.php';


    });
});
