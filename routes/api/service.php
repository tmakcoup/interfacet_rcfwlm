<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-28
 * Time: 10:36
 */

$api->group(['prefix' => 'service'],  function ($api){
    $api->post('addProject', ['as'=>'api.service.addProject','uses' => 'ServiceController@addProject']);
    $api->post('updProject', ['as'=>'api.service.updProject','uses' => 'ServiceController@updProject']);
    $api->get('delect', ['as'=>'api.service.delect','uses' => 'ServiceController@delect']);
    $api->get('list', ['as'=>'api.service.list','uses' => 'ServiceController@list']);
});
