<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-05-05
 * Time: 09:24
 */

//龙卡管理
$api->group(['prefix' => 'adminDragon'],  function ($api){
    $api->get('list1', ['as'=>'api.adminDragon.list1','uses' => 'AdminDragonController@list1']);
    $api->get('list2', ['as'=>'api.adminDragon.list2','uses' => 'AdminDragonController@list2']);
    $api->get('list3', ['as'=>'api.adminDragon.list3','uses' => 'AdminDragonController@list3']);
    $api->get('list4', ['as'=>'api.adminDragon.list4','uses' => 'AdminDragonController@list4']);
    $api->post('firstTrialDragon', ['as'=>'api.adminDragon.firstTrialDragon','uses' => 'AdminDragonController@firstTrialDragon']);
    $api->post('secondTrialDragon', ['as'=>'api.adminDragon.secondTrialDragon','uses' => 'AdminDragonController@secondTrialDragon']);
    $api->get('getBasics', ['as'=>'api.adminDragon.getBasics','uses' => 'AdminDragonController@getBasics']);
    $api->post('applyDragonCard', ['as'=>'api.adminDragon.applyDragonCard','uses' => 'AdminDragonController@applyDragonCard']);
    $api->post('updApplyDragonCard', ['as'=>'api.adminDragon.updApplyDragonCard','uses' => 'AdminDragonController@updApplyDragonCard']);
    $api->get('getUserZige', ['as'=>'api.adminDragon.getUserZige','uses' => 'AdminDragonController@getUserZige']);
    $api->get('getDetails', ['as'=>'api.adminDragon.getDetails','uses' => 'AdminDragonController@getDetails']);
    $api->get('removeApplyDragonCard', ['as'=>'api.adminDragon.removeApplyDragonCard','uses' => 'AdminDragonController@removeApplyDragonCard']);
    $api->post('bulkImport', ['as'=>'api.adminDragon.bulkImport','uses' => 'AdminDragonController@bulkImport']);
    $api->get('getMemberExcel', ['as'=>'api.adminDragon.getMemberExcel','uses' => 'AdminDragonController@getMemberExcel']);
});


//人才管理
$api->group(['prefix' => 'adminTalentsInfo'],  function ($api){
    $api->get('list', ['as'=>'api.adminTalentsInfo.list','uses' => 'AdminTalentsInfoController@list']);
    $api->post('checkTalents', ['as'=>'api.adminTalentsInfo.checkTalents','uses' => 'AdminTalentsInfoController@checkTalents']);
    $api->post('addTalents', ['as'=>'api.adminTalentsInfo.addTalents','uses' => 'AdminTalentsInfoController@addTalents']);
    $api->post('updTalents', ['as'=>'api.adminTalentsInfo.updTalents','uses' => 'AdminTalentsInfoController@updTalents']);
    $api->get('getUserZige', ['as'=>'api.adminTalentsInfo.getUserZige','uses' => 'AdminTalentsInfoController@getUserZige']);
    $api->get('getDetails', ['as'=>'api.adminTalentsInfo.getDetails','uses' => 'AdminTalentsInfoController@getDetails']);
    $api->get('removeTalents', ['as'=>'api.adminTalentsInfo.removeTalents','uses' => 'AdminTalentsInfoController@removeTalents']);
    $api->post('bulkImport', ['as'=>'api.adminTalentsInfo.bulkImport','uses' => 'AdminTalentsInfoController@bulkImport']);
    $api->get('getMemberTalentsExcel', ['as'=>'api.adminTalentsInfo.getMemberTalentsExcel','uses' => 'AdminTalentsInfoController@getMemberTalentsExcel']);
});


//业务办理
$api->group(['prefix' => 'adminBusinessHandling'],  function ($api){
    $api->get('list', ['as'=>'api.adminBusinessHandling.list','uses' => 'AdminBusinessHandlingController@list']);
    $api->get('getUserServiceList', ['as'=>'api.adminBusinessHandling.getUserServiceList','uses' => 'AdminBusinessHandlingController@getUserServiceList']);
    $api->get('accept', ['as'=>'api.adminBusinessHandling.accept','uses' => 'AdminBusinessHandlingController@accept']);
    $api->get('achieveAccept', ['as'=>'api.adminBusinessHandling.achieveAccept','uses' => 'AdminBusinessHandlingController@achieveAccept']);
    $api->post('updateProcess', ['as'=>'api.adminBusinessHandling.updateProcess','uses' => 'AdminBusinessHandlingController@updateProcess']);
});
