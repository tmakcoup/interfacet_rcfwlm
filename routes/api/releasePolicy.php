<?php
$api->group(['prefix' => 'releasePolicy'],  function ($api){
    $api->get('list', ['as'=>'api.releasePolicy.list','uses' => 'ReleasePolicyController@list']);
    $api->post('addReleasePolicy', ['as'=>'api.releasePolicy.addReleasePolicy','uses' => 'ReleasePolicyController@addReleasePolicy']);
    $api->post('updReleasePolicy', ['as'=>'api.releasePolicy.updReleasePolicy','uses' => 'ReleasePolicyController@updReleasePolicy']);
    $api->get('details', ['as'=>'api.releasePolicy.details','uses' => 'ReleasePolicyController@details']);
    $api->get('delete', ['as'=>'api.releasePolicy.delete','uses' => 'ReleasePolicyController@delete']);
    $api->get('getUpDown', ['as'=>'api.releasePolicy.getUpDown','uses' => 'ReleasePolicyController@getUpDown']);
});
