<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-28
 * Time: 17:37
 */

$api->group(['prefix' => 'dragon'],  function ($api){
    $api->get('getDetails', ['as'=>'api.dragon.getBasics','uses' => 'DragonController@getDetails']);
    $api->get('getBasics', ['as'=>'api.dragon.getBasics','uses' => 'DragonController@getBasics']);
    $api->get('reviewProgress', ['as'=>'api.dragon.reviewProgress','uses' => 'DragonController@reviewProgress']);
    $api->post('applyDragonCard', ['as'=>'api.dragon.applyDragonCard','uses' => 'DragonController@applyDragonCard']);
});

$api->group(['prefix' => 'talentsInfo'],  function ($api){
    $api->get('reviewProgress', ['as'=>'api.talentsInfo.reviewProgress','uses' => 'TalentsInfoController@reviewProgress']);
    $api->get('getDetails', ['as'=>'api.talentsInfo.getDetails','uses' => 'TalentsInfoController@getDetails']);
    $api->post('addTalents', ['as'=>'api.talentsInfo.addTalents','uses' => 'TalentsInfoController@addTalents']);
});

//
$api->group(['prefix' => 'reservation'],  function ($api){
    $api->get('getItems', ['as'=>'api.reservation.getItems','uses' => 'ReservationController@getItems']);
    $api->post('bookingService', ['as'=>'api.reservation.bookingService','uses' => 'ReservationController@bookingService']);
    $api->get('list', ['as'=>'api.reservation.list','uses' => 'ReservationController@list']);
    $api->get('details', ['as'=>'api.reservation.details','uses' => 'ReservationController@details']);
    $api->get('cancel', ['as'=>'api.reservation.cancel','uses' => 'ReservationController@cancel']);
    $api->get('subscribe', ['as'=>'api.reservation.cancel','uses' => 'ReservationController@subscribe']);
    $api->get('isHairpin', ['as'=>'api.reservation.isHairpin','uses' => 'ReservationController@isHairpin']);
});
