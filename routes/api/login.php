<?php
/**
 * Created by PhpStorm.
 * User: another
 * Date: 2019-04-24
 * Time: 16:29
 */
$api->group(['prefix' => 'login'],  function ($api){
    $api->get('adminLanding', ['as'=>'api.login.adminLanding','uses' => 'LoginController@adminLanding']);
    $api->get('apiLanding', ['as'=>'api.login.apiLanding','uses' => 'LoginController@apiLanding']);
    $api->post('retrievePassword', ['as'=>'api.login.retrievePassword','uses' => 'LoginController@retrievePassword']);
});
