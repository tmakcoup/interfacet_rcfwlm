/*
 Navicat MySQL Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : 127.0.0.1:3306
 Source Schema         : my_xuexi

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 28/04/2019 10:03:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rcjh_admins
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_admins`;
CREATE TABLE `rcjh_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台会员表';

-- ----------------------------
-- Records of rcjh_admins
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_admins` VALUES (1, '13627649153', '孔祥宝', '$2y$10$7PDjXFQVJVQf.YHybkmZP.RrWHE7DR2ljtpnerQJsel3a1Ax8hYoK', NULL, NULL, '2019-04-23 07:56:24', '2019-04-23 08:23:38');
INSERT INTO `rcjh_admins` VALUES (2, '13627649154', '孔祥宝', '$2y$10$aZwsniJOd4.13aEe/IAQ3ecwSCOflUm8.33QTaiPSef/I..u0mItK', NULL, NULL, '2019-04-23 08:24:14', '2019-04-24 07:08:22');
COMMIT;

-- ----------------------------
-- Table structure for rcjh_form_token
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_form_token`;
CREATE TABLE `rcjh_form_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `guard` tinyint(1) unsigned zerofill DEFAULT NULL COMMENT '1 admin.   2member',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `time` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='表单验证token';

-- ----------------------------
-- Records of rcjh_form_token
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_form_token` VALUES (1, 1, 2, 'task_token', '720b324300c95d269de48bffcb82de63', 1, '1556416155');
COMMIT;

-- ----------------------------
-- Table structure for rcjh_member
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_member`;
CREATE TABLE `rcjh_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台会员表';

-- ----------------------------
-- Records of rcjh_member
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_member` VALUES (1, '13627649153', '孔祥宝', '$2y$10$7PDjXFQVJVQf.YHybkmZP.RrWHE7DR2ljtpnerQJsel3a1Ax8hYoK', NULL, NULL, '2019-04-23 07:56:24', '2019-04-23 08:23:38');
INSERT INTO `rcjh_member` VALUES (2, '13627649154', '孔祥宝', '$2y$10$aZwsniJOd4.13aEe/IAQ3ecwSCOflUm8.33QTaiPSef/I..u0mItK', NULL, NULL, '2019-04-23 08:24:14', '2019-04-24 07:08:22');
COMMIT;

-- ----------------------------
-- Table structure for rcjh_member_codelog
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_member_codelog`;
CREATE TABLE `rcjh_member_codelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '1' COMMENT '1注册 2登陆 3修改资料',
  `mobile` varchar(20) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  `endtime` int(10) DEFAULT NULL COMMENT '过期时间',
  `updtime` int(10) DEFAULT NULL,
  `y_m_d` varchar(20) DEFAULT NULL COMMENT '判断今天验证码获取数量是否达到上限',
  `status` tinyint(1) DEFAULT '1' COMMENT '0已验证 1正常 2过期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='验证码发送记录';

-- ----------------------------
-- Records of rcjh_member_codelog
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_member_codelog` VALUES (1, 1, '13883833534', '536564', 1556178758, 1556179658, NULL, '2019-04-25', 1);
INSERT INTO `rcjh_member_codelog` VALUES (2, 1, '13883833534', '177467', 1556181208, 1556182108, NULL, '2019-04-25', 1);
INSERT INTO `rcjh_member_codelog` VALUES (3, 1, '13883833534', '928561', 1556184062, 1556184962, NULL, '2019-04-25', 1);
INSERT INTO `rcjh_member_codelog` VALUES (4, 1, '13883833534', '654682', 1556184154, 1556185054, NULL, '2019-04-25', 1);
INSERT INTO `rcjh_member_codelog` VALUES (5, 1, '13883833534', '576423', 1556184602, 1556185502, NULL, '2019-04-25', 1);
COMMIT;

-- ----------------------------
-- Table structure for rcjh_migrations
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_migrations`;
CREATE TABLE `rcjh_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rcjh_migrations
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `rcjh_migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `rcjh_migrations` VALUES (3, '2019_04_23_065820_entrust_setup_tables', 1);
COMMIT;

-- ----------------------------
-- Table structure for rcjh_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_password_resets`;
CREATE TABLE `rcjh_password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `rcjh_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for rcjh_permission_role
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_permission_role`;
CREATE TABLE `rcjh_permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `rcjh_permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `rcjh_permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `rcjh_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rcjh_permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `rcjh_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rcjh_permission_role
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_permission_role` VALUES (1, 3);
INSERT INTO `rcjh_permission_role` VALUES (2, 3);
INSERT INTO `rcjh_permission_role` VALUES (3, 3);
INSERT INTO `rcjh_permission_role` VALUES (1, 5);
INSERT INTO `rcjh_permission_role` VALUES (2, 5);
INSERT INTO `rcjh_permission_role` VALUES (3, 5);
INSERT INTO `rcjh_permission_role` VALUES (1, 6);
INSERT INTO `rcjh_permission_role` VALUES (2, 6);
INSERT INTO `rcjh_permission_role` VALUES (3, 6);
INSERT INTO `rcjh_permission_role` VALUES (1, 7);
INSERT INTO `rcjh_permission_role` VALUES (2, 7);
INSERT INTO `rcjh_permission_role` VALUES (3, 7);
INSERT INTO `rcjh_permission_role` VALUES (1, 8);
INSERT INTO `rcjh_permission_role` VALUES (2, 8);
INSERT INTO `rcjh_permission_role` VALUES (1, 9);
INSERT INTO `rcjh_permission_role` VALUES (2, 9);
INSERT INTO `rcjh_permission_role` VALUES (3, 9);
COMMIT;

-- ----------------------------
-- Table structure for rcjh_permissions
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_permissions`;
CREATE TABLE `rcjh_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '全路径',
  `level` tinyint(1) unsigned zerofill DEFAULT NULL COMMENT '等级',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rcjh_permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rcjh_permissions
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_permissions` VALUES (1, '0', '1', 0, 'api.index.index', '首页', '首页描述', NULL, '2019-04-23 09:53:21');
INSERT INTO `rcjh_permissions` VALUES (2, '1', '1-2', 1, 'api.index.add', '首页', '首页描述', NULL, '2019-04-23 09:53:48');
INSERT INTO `rcjh_permissions` VALUES (3, '2', '1-2-3', 2, 'api.index.sex', '首页', '首页描述', NULL, '2019-04-23 10:05:56');
COMMIT;

-- ----------------------------
-- Table structure for rcjh_role_user
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_role_user`;
CREATE TABLE `rcjh_role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `rcjh_role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `rcjh_role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `rcjh_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rcjh_role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `rcjh_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rcjh_role_user
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_role_user` VALUES (2, 3);
COMMIT;

-- ----------------------------
-- Table structure for rcjh_roles
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_roles`;
CREATE TABLE `rcjh_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rcjh_roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rcjh_roles
-- ----------------------------
BEGIN;
INSERT INTO `rcjh_roles` VALUES (1, '941d20', '技术部', '技术部', '2019-04-24 02:27:56', '2019-04-24 02:27:56');
INSERT INTO `rcjh_roles` VALUES (2, 'bdf183', '技术部', '技术部', '2019-04-24 02:28:39', '2019-04-24 02:28:39');
INSERT INTO `rcjh_roles` VALUES (3, 'f52cba', '技术部', '技术部', '2019-04-24 02:33:37', '2019-04-24 02:33:37');
INSERT INTO `rcjh_roles` VALUES (4, 'cca6b1', '技术部', '技术部', '2019-04-24 02:34:59', '2019-04-24 02:34:59');
INSERT INTO `rcjh_roles` VALUES (5, 'dfa61e', '技术部', '技术部', '2019-04-24 02:35:09', '2019-04-24 02:35:09');
INSERT INTO `rcjh_roles` VALUES (6, '0358bb', '技术部', '技术部', '2019-04-24 02:35:30', '2019-04-24 02:35:30');
INSERT INTO `rcjh_roles` VALUES (7, '9667d1', '技术部', '技术部', '2019-04-24 02:36:48', '2019-04-24 02:36:48');
INSERT INTO `rcjh_roles` VALUES (8, 'a3d8f1', '技术部', '技术部', '2019-04-24 02:38:27', '2019-04-24 03:47:10');
INSERT INTO `rcjh_roles` VALUES (9, '5f7cde', '技术部', '技术部', '2019-04-24 02:46:46', '2019-04-24 02:46:46');
COMMIT;

-- ----------------------------
-- Table structure for rcjh_service_project
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_service_project`;
CREATE TABLE `rcjh_service_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `describe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `time` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for rcjh_users
-- ----------------------------
DROP TABLE IF EXISTS `rcjh_users`;
CREATE TABLE `rcjh_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rcjh_users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
